<?
	error_reporting( error_reporting() & ~E_NOTICE );

	// Set up Connection
	$server = "localhost";			// Server
	$user 	= "production";				// Username
	$pass	= "1234";			// Password
	$db		= "production_erp";		// Database
	
	$sqlconn = mysql_connect($server,$user,$pass);
	if(!$sqlconn){
		echo "<h3> ERROR Cannot Connect Database</h3>";
		exit();
	}
	
	if(!mysql_select_db($db,$sqlconn)){
		echo "<h3> ERROR Cannot Select Database</h3>";
		exit();
	}
	
	$charset = "SET character_set_results=utf8";	// Set charset utf8
	mysql_query($charset) or die('Invalid query: ' . mysql_error());
	
	date_default_timezone_set("Asia/Bangkok");
	
	function num_to_month($inmonth){
		switch($inmonth){
		  case 1: return "January"; break;
		  case 2: return "February"; break;
		  case 3: return "March"; break;
		  case 4: return "April"; break;
		  case 5: return "May"; break;
		  case 6: return "June"; break;
		  case 7: return "July"; break;
		  case 8: return "August"; break;
		  case 9: return "September"; break;
		  case 10: return "October"; break;
		  case 11: return "November"; break;
		  case 12: return "December"; break;
		}
	}
?>