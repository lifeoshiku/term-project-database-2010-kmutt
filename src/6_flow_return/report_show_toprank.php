<?
	include "connect.inc.php";
	session_unset();
	$top = $_GET['top'];
	
	$sql = "select flow.Date, flowlineitem.MaterialCode, Product.Name, sum(flowlineitem.Quantity) as Quantity 
			from flowlineitem join Product on flowlineitem.MaterialCode = Product.ProductCode join flow on flow.FlowNo = flowlineitem.FlowNo ";
			
	if(isset($_GET['year']) || isset($_GET['month'])){
		$sql =  $sql . " where ";
	}
		
	// add year and month
	if(isset($_GET['year'])){
		$sql = $sql." year(Date)=".$_GET['year'];
		$text = "of Year ".$_GET['year'];
		
		if(isset($_GET['month'])){
			$sql = $sql." and month(Date)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])."/".$_GET['year'];
		}
			
	}else{
	
		if(isset($_GET['month'])){
			$sql = $sql." month(Date)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])." in every year";
		}
	}
	
	// group by
	$sql = $sql." group by flowlineitem.MaterialCode, Product.Name order by Quantity desc limit 0,".$top;
	//echo $sql;
	$res = mysql_query($sql,$sqlconn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Return Material :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(month != 0)
		url += "&month="+ month;
	if(year != 0)
		url += "&year="+ year;
		
	document.open(url,"Status","width=800 height=600");
}
</script>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center">
        <div style="font-size:14px; font-weight:bold;">:: Return Material top <?=$_GET['top'];?> <?=$text;?>::</div>
        </td>
      </tr>
      <tr>
        <td><br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="18%" height="32" align="center">Material Code</td>
              <td width="20%" align = "center" >Date</td>
              <td width="44%">Matetial Name</td>
              <td width="18%" align="center">Quantity</td>
              </tr>
            <?
				while($data = mysql_fetch_assoc($res)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center"><?=$data["MaterialCode"]?></td>
              <td align="center"><?=strftime("%d/%m/%Y",strtotime($data["Date"]));?></td>
              <td>&nbsp;<?=$data["Name"]?></td>
              <td align="center"><?=$data["Quantity"];?></td>
              </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
