<?
	include "connect.inc.php";
	session_unset();
	$top = $_GET['top'];
	$gettype = $_GET['type'];
	
	$sql =  "create view newtable as SELECT GoodHold.HoldDate, MOrder.ProductCode, Product.Name,  SUM(GoodHoldLineItem.QuantityIn)  AS QuantityIn FROM GoodHoldLineItem ".
			"JOIN MOrder ON GoodHoldLineItem.MONo = MOrder.MONo JOIN Product ON MOrder.ProductCode = Product.ProductCode ". 
			"JOIN GoodHold ON GoodHold.GoodHoldNo = GoodHoldLineItem.GoodHoldNo Group by  MOrder.ProductCode";
	mysql_query($sql,$sqlconn);
	
	$sql = 	"select n.HoldDate, m.ProductCode,n.Name,sum(m.Quantity) as Quantity,n.QuantityIn ,  n.QuantityIn / SUM( m.Quantity ) *100 AS Percentage from morder m join newtable n on m.ProductCode = n.ProductCode";
			
	if(isset($_GET['year']) || isset($_GET['month'])){
		$sql =  $sql . " where ";
	}
		
	// add year and month
	if(isset($_GET['year'])){
		$sql = $sql." year(HoldDate)=".$_GET['year'];
		$text = "of Year ".$_GET['year'];
		
		if(isset($_GET['month'])){
			$sql = $sql." and month(HoldDate)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])."/".$_GET['year'];
		}
			
	}else{
	
		if(isset($_GET['month'])){
			$sql = $sql." month(HoldDate)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])." in every year";
		}
	}
	if($gettype == "Number"){
		$sql = $sql." group by productcode  order by QuantityIn DESC limit 0,".$top;
	}else{
		$sql = $sql."  group by productcode  order by Percentage desc limit 0,".$top;
	}
		
	$res = mysql_query($sql,$sqlconn);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Good Hold :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(month != 0)
		url += "&month="+ month;
	if(year != 0)
		url += "&year="+ year;
		
	document.open(url,"Status","width=800 height=600");
}
function show_product_of_month(){
	var url = "report_show_toprank.php";
	var month = document.getElementById("month_c").value;
	var year = document.getElementById("year_c").value;
	var top = document.getElementById('txtNumber').value;
	

	if(top == "" || top == null || isNaN(top)){
		alert("Please input number !");
	}else{
		// add tail
		url += "?top="+top;
			
		if(month != 0)
			url += "&month="+ month;
		if(year != 0)
			url += "&year="+ year;
		
		document.open(url,"Toprank","width=850 height=600");
	}
}
function calculate_total(){
	
	var qp = document.getElementById("dummy1").value;
	var q =  document.getElementById("dummy2").value;
	document.getElementById("dummy3").value = (qp * q).toString();
}

</script>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center">
        <div style="font-size:14px; font-weight:bold;">:: Product Holded TOP <?=$_GET['top'];?> <?=$text;?>::</div>        </td>
      </tr>
      <tr>
        <td><br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="13%" align="center">Date</td>
              <td width="13%" height="32" align="center">Product Code</td>
              <td width="22%">Name</td>
              <td width="15%" align="center">Quantity</td>
              <td width="12%" align="center">Quantity In</td>
              <td width="24%" align="center">Percentage</td>
            </tr>
            <?
				while($data = mysql_fetch_assoc($res)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td align="center"><?=strftime("%d/%m/%Y",strtotime($data["HoldDate"]));?></td>
              <td height="28" align="center"><?=$data["ProductCode"]?></td>
              <td>&nbsp;
                  <?=$data["Name"]?></td>
              <td align="center"><?=$data["Quantity"];?></td>
              <td align="center"><?=$data["QuantityIn"];?></td>
              <td align="center"><?=number_format($data["Percentage"],2,'.',',' );?></td>
            </tr>
            <? } ?>
          </table>
          <p>&nbsp;</p></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
