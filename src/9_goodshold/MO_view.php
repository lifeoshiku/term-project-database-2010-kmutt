<?
	// index page
	session_start();
	include "connect.inc.php";
	session_unset();
	
	// delete transaction
	if($_GET["delete"]){
		$del_mo = $_GET["delete"];
		$sql = "select * from GoodHold where GoodHoldNo='$del_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$sql = "delete from GoodHoldLineItem where GoodHoldNo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			$sql = "delete from GoodHold where GoodHoldNo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			header("Location: mo_view.php");
		}
	}
	
	$get_mo = $_GET["mono"];
	
	if($get_mo!=""){
		$sql = "select GoodHoldNo,HoldDate, Reason, Location from GoodHold where GoodHoldNo='$get_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$_SESSION["GoodHoldNo"] = $get_mo;
			list($GoodHoldNo, $HoldDate, $Reason, $Location) = mysql_fetch_row($result);
			
			$sql = "select LocationName from location where Location = '$Location'";
			$res = mysql_query($sql);
			list($Location) = mysql_fetch_row($res);
			
			$HoldDate=strftime("%d/%m/%Y",strtotime($HoldDate));
			
			$menu_mode = "open";	
		}
	}else{
		$menu_mode = "new";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: Good Hold</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

function showmo_list() {
	window.open("select_mo.php", "HD", "width=320 height=600");
}

function delete_mo(mono){
	var answer = confirm("Do you want to delete Good Hold No # " + mono);
	if(answer){
		document.location.href = "mo_view.php?delete="+mono;
	}
	return false;
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="mo_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <? if($menu_mode == "open"){ ?>
            <td width="50"><div align="center">
            	<a href="mo_edit.php?mono=<?=$_SESSION["GoodHoldNo"]?>">
            		<img src="images/Modify.png" width="48" height="48" border="0" />
                </a>
            </div></td>
            <td width="50">
            <div align="center">
            	<a href="#">
       		    <img src="images/Delete.png" width="48" height="48" border="0" onclick="delete_mo('<?=$_SESSION["GoodHoldNo"]?>');" />
                </a>
            </div></td>
            <? } ?>
            <td width="50"><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <? if($menu_mode == "open"){ ?>
            <td><div align="center">Edit</div></td>
            <td><div align="center">Delete</div></td>
            <? } ?>
            <td><div align="center">Report</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="75" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Good Hold  Number :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default" id="txtTranNo" value="<?=$GoodHoldNo;?>" readonly="readonly" />
              <input name="button" type="submit" class="default_botton" id="button" value="..." onclick="showmo_list();" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$HoldDate;?>" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">Reason :</td>
            <td>
              <input name="txtStoreCode" type="text" class="default" id="txtStoreCode" value="<?=$Reason;?>" readonly="readonly" /></td>
            <td>Location :</td>
            <td>
              <input name="txtStoreName" type="text" class="default" id="txtStoreName" value="<?=$Location;?>" size="40" readonly="readonly" />              </td>
          </tr>
          <tr>
            <td height="25">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
          </tr>
          </table>
          <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="5%" height="32" align="center">&nbsp;#</td>
              <td width="10%" align="center">MO No</td>
              <td width="10%" align="center">Product Code</td>
              <td width="21%"><div align="center">Name</div></td>
              <td width="12%" align="center"><div align="right">Quantity</div></td>
              <td width="15%" align="center"><div align="right">Quantity In</div></td>
              <td width="7%" align="center">Unit</td>
              <td width="19%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            	$sql = "select G.Remark, G.MONo, P.Unit, G.QuantityIn, M.ProductCode, M.Quantity, P.Name ".
				       "from GoodHoldLineItem G join MOrder M on G.MONo = M.MONo join Product P on M.ProductCode = P.ProductCode ".
					   "where G.GoodHoldNo = '$GoodHoldNo'";
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;<?=$count;?></td>
              <td align="center"><?=$data["MONo"]?></td>
              <td align="center"><?=$data["ProductCode"]?></td>
              <td>&nbsp;<?=$data["Name"]?></td>
              <td align="right"><?=$data["Quantity"]?></td>
              <td align="right"><?=$data["QuantityIn"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              <td align="center"><?=$data["Remark"]?></td>
              <td>&nbsp;</td>
            </tr>
            <? } ?>
          </table>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
