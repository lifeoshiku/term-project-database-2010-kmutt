<?
	// index page
	session_start();
	include "connect.inc.php";
	
	// Check mode select
	if($_SESSION["Mode"]){
		$mode = $_SESSION["Mode"];
	}else{
		if(!isset($_GET["mono"])){
			$mode = "new";
		}else{
			$mode = "edit";
		}
	}	
	
	// clear session
	if(isset($_SESSION["isEditLine"])){
		$iseditline = $_SESSION["isEditLine"];
		if($iseditline && $_GET["cmd"] == "editline"){
			$iseditline = true;
		}else{
			unset($_SESSION["isEditLine"]);
			$iseditline = false;
		}
	}else{
		$iseditline = false;
	}
	
	$_SESSION["Mode"] = $mode;
	


	// Start Program
	// if new Mode
		if($mode == "new"){
		$GoodHoldNo = "NEW";
		$Reason = "Hold";
		if(isset($_SESSION["HoldDate"]))
			$HoldDate = $_SESSION["HoldDate"];	
		else
			$HoldDate = strftime("%d/%m/%Y",time());
		
	} 
	// if edit mode
	else if($mode == "edit" && !$_SESSION["isLoad"]) {
		// Get data from header
		if(isset($_GET["mono"]))
			$get_transaction = $_GET["mono"];
		else
			$get_transaction = $_SESSION["MONo"];
		
		$sql = "select GoodHoldNo,HoldDate, Reason, Location from GoodHold where GoodHoldNo='$GoodHoldNo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			list($GoodHoldNo, $HoldDate, $Reason, $Location) = mysql_fetch_row($result);
			$HoldDate = strftime("%d/%m/%Y",strtotime($HoldDate));

			
			// Read Line item
			$sql = "select G.Remark, G.MONo, P.Unit, G.QuantityIn, M.ProductCode, M.Quantity, P.Name ".
				       "from GoodHoldLineItem G join MOrder M on G.MONo = M.MONo join Product P on M.ProductCode = P.ProductCode ".
					   "where G.GoodHoldNo = '$GoodHoldNo'";
			$result = mysql_query($sql,$sqlconn);
			while($data = mysql_fetch_assoc($result)){
				//$data["Date"] = strftime("%d/%m/%Y",strtotime($data["Date"]));
				$LineItem[] = $data;
			}
			$_SESSION["LineItem"] = $LineItem;
			$_SESSION["isLoad"] = 1;
			
			// Save Session
			$_SESSION["GoodHoldNo"] = $GoodHoldNo;
			$_SESSION["HoldDate"] = $HoldeDate;
			$_SESSION["Reason"] = $Reason;
			$_SESSION["Location"] = $Location;				
		}
	}
	else{
		$GoodHoldNo = $_SESSION["GoodHoldNo"];
		$HoldDate = $_SESSION["HoldDate"];
		$Reason = $_SESSION["Reason"];
	}
	
		
	$Location = $_SESSION["Location"];

	
	// Product Select
	if(isset($_GET["materialcode"])){
		$Mat = $_GET["materialcode"];
		$sql = "select M.MONo, M.ProductCode, P.Name, M.Quantity, P.Unit from MOrder M join Product P ".
		"on M.ProductCode = P.ProductCode where MONo='$Mat'";	
		
		
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result) != 0){
			list($MONo,$ProductCode,$Name,$Quantity,$Unit) = mysql_fetch_row($result);
		}
	}
	
	// Keep Line Item
	if($_SESSION["LineItem"]){
		$LineItem = $_SESSION["LineItem"];
	}
	
	// Process Command
	if($_GET["cmd"]){
		if($_GET["cmd"] == "save"){
			// New
			if($mode == "new"){
				// generate running number
				$sql = "select LastNumber from runningnumber where RunTable='HD'";
				$result = mysql_query($sql, $sqlconn);
				list($next_no) = mysql_fetch_row($result);
				
				$GoodHoldNo = "HD".$next_no;
				
				// increase next number
				$sql = "update runningnumber set LastNumber=LastNumber+1 where RunTable='HD'";
				$result = mysql_query($sql, $sqlconn);

				// insert new header
				list($add_day,$add_month,$add_year) = split("/",$HoldDate);
				$DateSql = "$add_year-$add_month-$add_day";
				$sql = "insert into GoodHold values ".
						"('$GoodHoldNo','$DateSql','$Reason','$Location')";
				$result = mysql_query($sql, $sqlconn);
				
				$mode = "edit";
			}
			// Old
			else{
				list($day,$month,$year) = split("/",$HoldDate);
				$DateSql = "$year-$month-$day";
				$sql = "update GoodHold set GoodHoldNo='$GoodHoldNo',HoldDate='$DateSql',".
						"Reason='$Reason', Location='$Location' where ".
						"GoodHoldNo='$GoodHoldNo'";
				$result = mysql_query($sql, $sqlconn);
				
				
				// delete old lineitem
				$sql = "delete from GoodHoldLineItem where GoodHoldNo='$GoodHoldNo'";
				$result = mysql_query($sql, $sqlconn);
			}
			
			// insert new lineitem	
			if(count($_SESSION["LineItem"]) > 0){
				foreach($_SESSION["LineItem"] as $row){
					$mo_mn = $row["MONo"];
					$mo_qi = $row["QuantityIn"];
					$mo_remark = $row["Remark"];
					
					$sql = "insert into GoodHoldLineItem values ('$GoodHoldNo','$mo_mn','$mo_qi'".				
							",'$mo_remark')";
					mysql_query($sql, $sqlconn);
				}
			}
		}
		// remove line item command
		else if($_GET["cmd"] == "removeline"){
			if(count($LineItem)>0){
				$removeat = $_GET["line"];
				foreach($LineItem as $row){
					if($removeat == $row["MONo"]){
						continue;
					}else{
						$tmp_LineItem[] = $row;
					}
				}
				$LineItem = $tmp_LineItem;
				$_SESSION["LineItem"] = $LineItem;
				unset($tmp_LineItem);
			}
		}
		
		// edit line item
		else if($_GET["cmd"] == "editline"){
			if(count($LineItem)>0){
				$editat = $_GET["line"];
				foreach($LineItem as $row){
					if($editat == $row["MONo"]){
						$MONo = 	$row["MONo"];
						$ProductCode = 	$row["ProductCode"];
						$Name = 	$row["Name"];
						$Quantity =		$row["Quantity"];
						$QuantityIn =	$row["QuantityIn"];
						$Unit = 			$row["Unit"];
						$Remark  =			$row["Remark"];
						$_SESSION["isEditLine"] = true;
						$iseditline = true;
						break;
					}
				}
			}
		}
	}
	
	// datatable session
	if($_POST["btn_add"]){
		// add new lineitem
		if($_POST["btn_add"] == "Add"){
			// check same appcode
			$already_data = false;
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["MONo"] == $_POST["txtMONo"])
						$already_data = true;
				}
			}
			
			if(!$already_data){
			
				$new_lineitem = array(
					"MONo" => $_POST["txtMONo"],
					"ProductCode" => $_POST["txtProductCode"],
					"Name" => $_POST["txtName"],
					"Quantity" => $_POST["txtQuantity"],
					"QuantityIn" => $_POST["txtQuantityIn"],
					"Unit" => $_POST["txtUnit"],
					"Remark" => $_POST["txtRemark"],
				);
				
				$LineItem[] = $new_lineitem;
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$MONo = "";
			$ProductCode = "";
			$Name = "";
			$Unit = "";
			$Quantity ="";
			$QuantityIn = "";

		}
		// edit line item
		else{
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["MONo"] == $_POST["txtMONo"]){
						$new_lineitem = array(
							"MONo" => $_POST["txtMONo"],
							"ProductCode" => $_POST["txtProductCode"],
							"Name" => $_POST["txtName"],
							"Quantity" => $_POST["txtQuantity"],
							"QuantityIn" => $_POST["txtQuantityIn"],
							"Unit" => $_POST["txtUnit"],
							"Remark" => $_POST["txtRemark"],
						);
						$tmp_LineItem[] = $new_lineitem;
					}else{
						$tmp_LineItem[] = $row;
					}	
				}
				
				// fill session
				$LineItem = $tmp_LineItem;
				unset($tmp_LineItem);
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$_SESSION["isEditLine"] = false;
			$iseditline = false;
			
			$MONo = "";
			$ProductCode = "";
			$Name = "";
			$Unit = "";
			$Quantity ="";
			$QuantityIn = "";
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: GoodHold</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

<?
	if($already_data){
		echo "alert(\"Already Data #".$_POST["txtMONo"]."\");";
	}
	
	if($isnotinputproduct){
		echo "alert(\"Select Store !\");";
	}
?>

var old_date = "";
var Interval;
var count;
// ajax function
function newXmlHttp(){
	var xmlhttp = false;
	
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlhttp = false;
		}
	}
	
	if(!xmlhttp && document.createElement){
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
function update_date(){
	old_date = document.getElementById('txtDate').value;
	Interval = setInterval('update()',500);
	count = 0;
}
function update(){
	var date = document.getElementById("txtDate").value;
	var url = "";
	count ++;
	if(count == 20){
		clearInterval(Interval);
		count = 0;
	}
	if(date != old_date){
		url = "update.php?date="+date;
		//alert(url);
		xmlhttp = newXmlHttp();
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		
		clearInterval(Interval);
		count = 0;
	}
}

function update_quantity(){
	var quantity = document.getElementById("txtQuantityIn").value;
	var url = "";
	url = "update.php?quantity="+quantity;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

function update_location(){
	var location = document.getElementById("txtLocation").value;
	var url = "";
	url = "update.php?location="+location;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

// end function

function showproduct_list() {
	window.open("select_product.php", "Product", "width=370 height=600");
}

function showmaterial_list() {
	window.open("select_material.php", "Material", "width=370 height=600");
}

function clear_app(){
	document.getElementById("txtProductCode").value = "";
	document.getElementById("txtName").value = "";
	document.getElementById("txtMONo").value = "";
	document.getElementById("txtQuantity").value = "";
	document.getElementById("txtQuantityIn").value = "";
	document.getElementById("txtUnit").value = "";
	document.getElementById("txtRemark").value = "";
}

function submit_header(){
	var location = document.getElementById('txtLocation').value;
	if(location == "None"){
		alert("Select Location");
		return false;
	}else{
		document.location.href = "mo_edit.php?cmd=save";
		return true;
	}
}

function check_blank(){
	var mo = document.getElementById("txtMONo").value;
	var qin = document.getElementById("txtQuantityIn").value;
	var remarks = document.getElementById("txtRemark").value;
	
	if(mo == ""){
		alert("Please choose MO");
		return false;
	}
	if(qin == ""){
		alert("Please input Quantity in");
		return false;
	}
	if(remarks == ""){
		document.getElementById("txtRemark").value = "-";
	}
	
	return true;
}

function delete_lineitem(matcode){
	var answer = confirm("Do you want to delete GoodHold LineItem with MONo# " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=removeline&line="+matcode;
	}
	return false;
}
function edit_lineitem(matcode){
	var answer = confirm("Do you want to edit GoodHold LineItem with MONo# " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=editline&line="+matcode;
	}
	return false;
}
function cancel_edit(){
	document.location.href = "mo_edit.php";
}

function check_quantityin(){
	var qan = document.getElementById("txtQuantity").value;
	var qin = document.getElementById("txtQuantityIn").value;
	if((qan - qin) < 0){
		alert("Quantity in must be less than Quantity.");
		//document.getElementById("txtQuantityIn").value = "";
	}
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table width="160" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            <div align="center">
				<a href="#" onclick="submit_header();">
            	<img src="images/Save.png" width="48" height="48" border="0"/>                </a>            </div>            
              </td>
            <td>
            <div align="center">
            	<a href="mo_view.php">
                <img src="images/Cancel.png" width="48" height="48" border="0" />                </a>            </div>            </td>
            <td><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
          </tr>
          <tr>
            <td><div align="center">Save</div></td>
            <td><div align="center">Cancel</div></td>
            <td><div align="center">Report</div></td>
          </tr>
        </table>        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="75" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Good Hold  Number :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default_auto" id="txtTranNo" value="<?=$GoodHoldNo;?>" readonly="readonly" /></td>
            <td width="13%"> Hold Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$HoldDate;?>" onfocus="showCalendarControl(this);" onblur="update_date();" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">Reason :</td>
            <td>
              <input name="txtStoreCode" type="text" class="default_auto" id="txtStoreCode" value="<?=$Reason;?>" readonly="readonly" /></td>
            <td>Location :</td>
            <td>  <select name="txtLocation" class="default" id="txtLocation" onchange="update_location();">
              <option value="None" <? if($Location == "None") echo "selected=\"selected\"";?>>None</option>
				<?	
			  		$sql = "select * from location";
			  		$res = mysql_query($sql,$sqlconn);
					while($data = mysql_fetch_assoc($res)){
						echo "<option value=\"".$data["Location"]."\"";
						if($Location == $data["Location"])
							echo "selected=\"selected\"";
						echo ">".$data["LocationName"]."</option>";
					} 
				?>
            </select></td>
          </tr>
          <tr>
            <td height="25">&nbsp;</td>
            
            <td colspan="3"><label></label></td>
          </tr>
          </table>
          
        <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="5%" height="35">&nbsp;</td>
              <td width="5%">&nbsp;</td>
              <td width="8%" align="center">MO No</td>
              <td width="8%" align="center">Product Code</td>
              <td width="20%">Name</td>
              <td width="11%" align="center"><div align="right">Quantity</div></td>
              <td width="9%" align="center"> <div align="right">Quantity In</div></td>
              <td width="7%" align="center"><div align="right">Unit</div></td>
              <td width="20%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            if (count($LineItem)>0)
			{
				$index = 0;
				$style = "even";
				foreach($LineItem as $row)
				{
					$index ++;
					if($style == "even")  $style = "odd";
					else $style = "even";
			?>
            <tr class="<?=$style;?>">
              <td align="center" height="28"><img src="images/editpic.png" width="20" height="20" onclick="edit_lineitem('<?=$row["MONo"];?>');"/></td>
              <td align="center"><img src="images/delpic.png" width="20" height="20" onclick="delete_lineitem('<?=$row["MONo"];?>');"/></td>
              <td align="center"><?=$row["MONo"];?></td>
              <td align="center"><?=$row["ProductCode"];?></td>
              <td><?=$row["Name"];?></td>
              <td align="right"><?=$row["Quantity"];?></td>
              <td align="right"><?=$row["QuantityIn"];?></td>
              <td align="right"><?=$row["Unit"];?></td>
              <td align="center"><?=$row["Remark"];?></td>
              <td>&nbsp;</td>
            </tr>
            <? } 
			} ?>
          </table>
          <br />
          <br />
          <form name="NewApp" method="post">
          <table border="0" width="96%" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <b>
                    <?
                    if(!$iseditline){
						echo "Add new Good Hold Line Item:"; 
						$btn_value = "Add";
					}else{
						echo "Edit Good Hold Line Item:";
						$btn_value = "Edit";
					}
					?>
                    </b></td>
            </tr>
            <tr>
                <td>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr valign="bottom"> 
                        <td width="134" height="19">MO No</td>   
                      <td width="102">Product  Code</td>  
                      <td width="171">Product  Name</td>  
                      <td width="64">Quantity</td>  
                      <td width="64">QuantityIn</td>  
                      <td width="67">Unit</td>   
                      <td>Remarks</td>                                  
                    </tr>
                    <tr>
                        <td height="34"><input name="txtMONo" type="text" class="default" id="txtMONo" value="<?=$MONo;?>" size="15" readonly="readonly" />
                        <? if(!$iseditline){
                        echo "<input name=\"button3\" type=\"button\" class=\"default_botton\" id=\"button3\" value=\"...\" onclick=\"showmaterial_list();\" />";
                         } ?>                        </td>   
             			 <td>
                            <input name="txtProductCode" type="text" class="default_auto" id="txtProductCode" value="<?=$ProductCode;?>" size="15" readonly="readonly"/></td>  
                        <td><input name="txtName" type="text" class="default_auto" id="txtName" value="<?=$Name;?>" size="28" readonly="readonly"/></td>  
                      <td><input name="txtQuantity" type="text" class="default_auto" id="txtQuantity" value="<?=$Quantity;?>" size="6" readonly="readonly"/></td>  
                        <td>
                            <input name="txtQuantityIn" type="text" class="default" id="txtQuantityIn" value="<?=$QuantityIn;?>" size="6" onchange="check_quantityin()"/></td>  
                        <td>
                            <input name="txtUnit" type="text" class="default_auto" id="txtUnit" value="<?=$Unit;?>" size="6" readonly="readonly"/></td> 
                        <td width="166">
                        <input name="txtRemark" type="text" class="default" id="txtRemark" value="<?=$Remark;?>" size="30" /></td> 
                    </tr>
                </table>
                 <?
                    if($iseditline){
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Edit\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"cancel\" onclick=\"cancel_edit();\");\" />"; 
				 
					}else{
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Add\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"Clear\" onclick=\"clear_app();\" />"; 
					}
				 ?>                </td>
            </tr>
        </table>
        </form>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
