<?
	include "connect.inc.php";
	session_unset();
	$gettype = $_GET['type'];
	
	// select command
		$sql = "SELECT GoodHold.GoodHoldNo, GoodHold.HoldDate, MOrder.ProductCode, Product.Name, GoodHoldLineItem.QuantityIn, ".
		       "Product.Unit FROM GoodHold INNER JOIN  GoodHoldLineItem ON GoodHold.GoodHoldNo = GoodHoldLineItem.GoodHoldNo ".
			   "INNER JOIN MOrder ON GoodHoldLineItem.MONo = MOrder.MONo INNER JOIN ".
			   "Product ON MOrder.ProductCode = Product.ProductCode ";
		if(isset($_GET['year']) || isset($_GET['month']))
			$sql =  $sql . " where ";
			
		// add year and month
		if(isset($_GET['year'])){
			$sql = $sql." year(HoldDate)=".$_GET['year'];
			$text = "of Year ".$_GET['year'];
			
			if(isset($_GET['month'])){
				$sql = $sql." and month(HoldDate)=".$_GET['month'];
				$text = "of ".num_to_month($_GET['month'])."/".$_GET['year'];
			}
				
		}else{
			if(isset($_GET['month'])){
				$sql = $sql." month(HoldDate)=".$_GET['month'];
				$text = "of ".num_to_month($_GET['month'])." in every year";
			}
		}

	
	
	$res = mysql_query($sql,$sqlconn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Good Hold:: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(month != 0)
		url += "&month="+ month;
	if(year != 0)
		url += "&year="+ year;
		
	document.open(url,"Status","width=800 height=600");
}
</script>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center"><div style="font-size:14px; font-weight:bold;">:: Good Hold Report <?=$text;?> ::</div></td>
      </tr>
      <tr>
        <td><br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="20%" height="32" align="center">Good Hold No</td>
              <td width="23%"><div align="center">Hold Date</div></td>
              <td width="15%" align="center">Product Code</td>
              <td width="17%" align="center">Name</td>
              <td width="17%" align="center">Quantity In</td>
              <td width="15%" align="center">Unit</td>
              </tr>
            <?
				while($data = mysql_fetch_assoc($res)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center"><?=$data["GoodHoldNo"]?></td>
              <td align="center"><?=strftime("%d/%m/%Y",strtotime($data["HoldDate"]));?></td>
              <td>&nbsp;<?=$data["ProductCode"]?></td>
              <td align="center"><?=$data["Name"]?></td>
              <td align="center"><?=$data["QuantityIn"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
