<?
	session_start();
	include "connect.inc.php";
	session_unset();
	
	$sql = "CREATE VIEW findyear_inline AS SELECT year( Date ) AS year FROM flow GROUP BY year; ";
	mysql_query($sql,$sqlconn);
	
	$sql = "SELECT min( year ) as minyear , max( year ) as maxyear FROM findyear_inline";
	$res = mysql_query($sql,$sqlconn);
	list($min,$max) = mysql_fetch_row($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Goods Move in line (Output) :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_select_mo(){
	document.open("report_show_mo.php","MO","width=850 height=600");
}

function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(prme != "all"){
		if(month != 0)
			url += "&month="+ month;
		if(year != 0)
			url += "&year="+ year;
	}
		
	document.open(url,"Status","width=850 height=600");
}

function show_product_of_month(){
	var url = "report_show_toprank.php";
	var month = document.getElementById("month_c").value;
	var year = document.getElementById("year_c").value;
	
	var top = prompt("Please enter number of rank. Ex. 10 for Top 10.","5");

	if(top == "" || top == null || isNaN(top)){
		alert("Please input number !");
	}else{
		// add tail
		url += "?top="+top;
			
		if(month != 0)
			url += "&month="+ month;
		if(year != 0)
			url += "&year="+ year;
		
		document.open(url,"Toprank","width=850 height=600");
	}
}
</script>
<style type="text/css">
<!--
.style2 {font-size: 14px; font-weight: bold; }
-->
</style>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="output_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <td width="50">
            <div align="center"><a href="output_view.php"><img src="images/Modify.png" width="48" height="48" border="0" /></a></div>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <td><div align="center">Open</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td>
        	
          <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
            <tr>
              <td class="show_header"><h2 align="center">Select Report View</h2></td>
            </tr>
            <tr>
              <td><br />
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="30%" align="center"><p><img src="images/Print.png" width="48" height="48" /></p>
                      <p><a href="report_show_ot.php" class="style2">Output Flow No. Report</a></p></td>
                    <td width="30%" align="center"><p><img src="images/Print.png" alt="" width="48" height="48" /></p>
                      <p><a href="report_show_pm.php" class="style2">Location by Month Report</a></p></td>
                    <td width="30%" align="center"><p><img src="images/Print.png" alt="" width="48" height="48" /></p>
                      <p><a href="report_show_top.php" class="style2">Top n Report</a></p></td>
                </tr>
              </table>
              </td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <br />          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
