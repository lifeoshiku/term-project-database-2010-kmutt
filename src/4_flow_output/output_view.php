<?
	// index page
	session_start();
	include "connect.inc.php";
	session_unset();
	
	// delete transaction
	if($_GET["delete"]){
		$del_flow = $_GET["delete"];
		$sql = "select * from flow where FlowNo='$del_flow'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$sql = "delete from flowlineitem where FlowNo='$del_flow'";
			mysql_query($sql, $sqlconn);
			
			$sql = "delete from flow where FlowNo='$del_flow'";
			mysql_query($sql, $sqlconn);
			
			header("Location: output_view.php");
		}
	}
	
	$get_mo = $_GET["flowno"];
	
	if($get_mo!=""){
		$sql = "select FlowNo,Date,Location from flow where FlowNo='$get_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$_SESSION["FlowNo"] = $get_mo;
			$Reason = "Output"; 	// Set reason
			list($FlowNo,$Date,$Location) = mysql_fetch_row($result);
			
			$sql = "select LocationName from location where Location = '$Location'";
			$res = mysql_query($sql);
			list($Location) = mysql_fetch_row($res);
			
			$Date=strftime("%d/%m/%Y",strtotime($Date));
			$menu_mode = "open";	
		}
	}else{
		$menu_mode = "new";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: Goods Move in Line (Output)</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

function showflow_list() {
	window.open("select_flow.php", "FLOW", "width=320 height=600");
}

function delete_flow(no){
	var answer = confirm("Do you want to delete Flow No # " + no);
	if(answer){
		document.location.href = "output_view.php?delete="+no;
	}
	return false;
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="output_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>            </td>
            <? if($menu_mode == "open"){ ?>
            <td width="50"><div align="center">
            	<a href="output_edit.php?flowno=<?=$_SESSION["FlowNo"]?>">
            		<img src="images/Modify.png" width="48" height="48" border="0" />                </a>
            </div></td>
            <td width="50">
            <div align="center">
            	<a href="#">
       		    <img src="images/Delete.png" width="48" height="48" border="0" onclick="delete_flow('<?=$_SESSION["FlowNo"]?>');" />                </a>            </div></td>
            <? } ?>
            <td width="50"><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <? if($menu_mode == "open"){ ?>
            <td><div align="center">Edit</div></td>
            <td><div align="center">Delete</div></td>
            <? } ?>
            <td><div align="center">Report</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="50" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Flow Number :</td>
            <td width="26%">
              <input name="txtFlowNo" type="text" class="default" id="txtFlowNo" value="<?=$FlowNo;?>" readonly="readonly" />
              <input name="button" type="submit" class="default_botton" id="button" value="..." onclick="showflow_list();" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">Reason :</td>
            <td>
              <input name="txtReason" type="text" class="default" id="txtReason" value="<?=$Reason;?>" readonly="readonly" /></td>
            <td>Location :</td>
            <td><input name="txtLocation" type="text" class="default" id="txtLocation" value="<?=$Location;?>" size="20" /></td>
          </tr>
          </table>
          <br />          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="7%" height="32" align="center">&nbsp;#</td>
              <td width="15%" align="center">MO No</td>
              <td width="10%" align="center">Material Code</td>
              <td width="25%" align="center">Name</td>
              <td width="11%" align="center">Quantity</td>
              <td width="8%" align="center">Unit</td>
              <td width="23%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            	$sql = "select F.FlowNo,F.MONo,F.MaterialCode,F.Quantity,F.Remarks".
				       ",P.Name,P.Unit from flowlineitem F join Product P on F.MaterialCode = P.ProductCode ".
					   "where F.FlowNo = '$FlowNo'";
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;<?=$count;?></td>
              <td align="center"><?=$data["MONo"]?></td>
              <td align="center">&nbsp;<?=$data["MaterialCode"]?></td>
              <td><?=$data["Name"]?></td>
              <td align="right"><?=$data["Quantity"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              <td align="center"><?=$data["Remarks"]?></td>
              <td>&nbsp;</td>
            </tr>
            <? } ?>
          </table>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
