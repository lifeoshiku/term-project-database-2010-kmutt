<?
	session_start();
	session_unset();
	include "connect.inc.php";
	$sql = "CREATE VIEW findyear_rqa AS SELECT year( Date ) AS year FROM  receivingqa GROUP BY year; ";
	mysql_query($sql,$sqlconn);
	
	$sql = "SELECT min( year ) as minyear , max( year ) as maxyear FROM findyear_rqa";
	$res = mysql_query($sql,$sqlconn);
	list($min,$max) = mysql_fetch_row($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RQA :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_select_qa(){
	document.open("report_show_qa.php","RQA","width=850 height=600");
}

function show_product_each_month(){
	var url = "report_show_eachmonth.php?";
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
		if(month != 0)
			url += "&month="+ month;
		if(year != 0)
			url += "&year="+ year;
		
		document.open(url,"Monthly Report","width=850 height=600");
}

function show_sum_of_product(){
	var url = "report_sum_product.php?";
	var month = document.getElementById("month_c").value;
	var year = document.getElementById("year_c").value;
		
		if(month != 0)
			url += "&month="+ month;
		if(year != 0)
			url += "&year="+ year;
		
		document.open(url,"Sum of Product","width=850 height=600");
}
</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="mo_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <td width="50">
            <div align="center"><a href="mo_view.php"><img src="images/Modify.png" width="48" height="48" border="0" /></a></div>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <td><div align="center">Open</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td><br />
          <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
            <tr>
              <td class="show_header">View Report ::</td>
            </tr>
            <tr>
              <td>
              <br />
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr valign="top">
                  <td align="center" valign="top"><b>Receiving QA Report</b></td>
                  <td align="center" valign="top" style="border-left:dotted 1px #333333;"><b>Monthly Report</b></td>
                  <td align="center" valign="top" style="border-left:dotted 1px #333333;"><b>Sum of Product for QA</b></td>
                </tr>
                <tr valign="top">
                  <td width="33%" rowspan="2" align="center" valign="top"><input name="button4" type="button" class="default_botton" id="button4" value="Select RQA" onclick="show_select_qa();" /></td>
                  <td height="75" align="center" valign="top" style="border-left:dotted 1px #333333;"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="19" colspan="2">Select Date :</td>
                    </tr>
                    <tr>
                      <td width="32%" height="28">&nbsp;&nbsp;&nbsp;Month</td>
                      <td width="68%"><select name="month_b" class="default" id="month_b">
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                        <option value="0" selected="selected">---</option>
                      </select>                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;&nbsp;&nbsp;Year</td>
                      <td><select name="year_b" class="default" id="year_b">
                      <option value="0" selected="selected">---</option>
                      <?
                      for($i=$min;$i<=$max;$i++)
					  	echo "<option value=\"$i\">$i</option>";
					  ?>
                      </select>                      </td>
                    </tr>

                  </table></td>
                  <td align="center" valign="top" style="border-left:dotted 1px #333333;"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="19" colspan="2">Select Date :</td>
                    </tr>
                    <tr>
                      <td width="32%" height="28">&nbsp;&nbsp;&nbsp;Month</td>
                      <td width="68%"><select name="month_c" class="default" id="month_c">
                          <option value="1">January</option>
                          <option value="2">February</option>
                          <option value="3">March</option>
                          <option value="4">April</option>
                          <option value="5">May</option>
                          <option value="6">June</option>
                          <option value="7">July</option>
                          <option value="8">August</option>
                          <option value="9">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                          <option value="0" selected="selected">---</option>
                        </select>                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;&nbsp;&nbsp;Year</td>
                      <td><select name="year_c" class="default" id="year_c">
                      <option value="0" selected="selected">---</option>
                      <?
                      for($i=$min;$i<=$max;$i++)
					  	echo "<option value=\"$i\">$i</option>";
					  ?>
                      </select>                      </td>
                    </tr>
                  </table></td>
                </tr>
                <tr valign="top">
                  <td width="33%" align="center" valign="top" style="border-left:dotted 1px #333333;"><input width="70px" name="button3" type="button" class="default_botton" id="button3" value="Show Report" onclick="show_product_each_month();"/>
                    <br /></td>
                  <td width="34%" align="center" valign="top" style="border-left:dotted 1px #333333;"><input name="button5" type="button" class="default_botton" id="button5" value="Show Report" onclick="show_sum_of_product();" /></td>
                </tr>
              </table>
              </br>
              </td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <br />          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
