<?
	// index page
	session_start();
	include "connect.inc.php";
	
	// Check mode select
	if($_SESSION["Mode"]){
		$mode = $_SESSION["Mode"];
	}else{
		if(!isset($_GET["qano"])){
			$mode = "new";
			$_SESSION['Status'] = "WO";
		}else{
			$mode = "edit";
		}
	}	
	
	// clear session
	if(isset($_SESSION["isEditLine"])){
		$iseditline = $_SESSION["isEditLine"];
		if($iseditline && $_GET["cmd"] == "editline"){
			$iseditline = true;
		}else{
			unset($_SESSION["isEditLine"]);
			$iseditline = false;
		}
	}else{
		$iseditline = false;
	}
	
	$_SESSION["Mode"] = $mode;
	
	// PO/WO Select
	if(isset($_GET["tranno"])){
		$TranNo = $_GET["tranno"];
		if($Status=="PO")
		{
			$sql = "select PONo,Supplier from purchaseorder where PONo='$TranNo'";
			$result = mysql_query($sql, $sqlconn);
			$Chosen = "PO";
		}
		else
		{
			$sql = "select WONo,Supplier from  workorder where WONo='$TranNo'";
			$result = mysql_query($sql, $sqlconn);
			$Chosen = "WO";
		}
		if(mysql_num_rows($result) != 0){
			list($TranNo,$Supplier) = mysql_fetch_row($result);
			$_SESSION["TranNo"] = $TranNo;
			$_SESSION["Supplier"] = $Supplier;
			$_SESSION["Chosen"] = $Chosen;
		}
	}
	
	// Start Program
	// if new Mode
	if($mode == "new"){
		$QANo = "NEW";
		if(isset($_SESSION["Date"]))
			$Date = $_SESSION["Date"];	
		else
			$Date = strftime("%d/%m/%Y",time());
	} 
	// if edit mode
	else if($mode == "edit" && !$_SESSION["isLoad"]) {
		// Get data from header
		if(isset($_GET["qano"]))
			$get_transaction = $_GET["qano"];
		else
			$get_transaction = $_SESSION["QANo"];
		
		$sql = "select ReceivingQANo,Date,PurchaseNo,WorkOrderNo from receivingqa where ReceivingQANo='$get_transaction'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			list($QANo,$Date,$PurchaseNo,$WorkOrderNo) = mysql_fetch_row($result);
			if($WorkOrderNo=="")
			{
				$TranNo = $PurchaseNo;
				$sql = "select Supplier from purchaseorder where PONo='$PurchaseNo'";
				$result = mysql_query($sql, $sqlconn);
				list($Supplier) = mysql_fetch_row($result);
				$Chosen = "PO";
			}
			else
			{
				$TranNo = $WorkOrderNo;
				$sql = "select Supplier from workorder where WONo='$WorkOrderNo'";
				$result = mysql_query($sql, $sqlconn);
				list($Supplier) = mysql_fetch_row($result);
				$Chosen = "WO";
			}
			$_SESSION["Chosen"] = $Chosen;
			
			$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			// Read Line item
			$sql = "select M.MaterialCode as ProductCode,M.TotalQty as TotalQuantity".
				   ",M.QtyGood as QuantityGood,M.QtyReturn as QuantityReturn,M.Remark as Remarks".
				   ",P.Name,P.MaterialType,P.Unit from receivingqalineitem M join Product P on M.MaterialCode = P.ProductCode ".
				   "where M.ReceivingQANo = '$QANo'";
			$result = mysql_query($sql,$sqlconn);
			while($data = mysql_fetch_assoc($result)){
				$LineItem[] = $data;
			}
			$_SESSION["LineItem"] = $LineItem;
			$_SESSION["isLoad"] = 1;
			
			// Save Session
			$_SESSION["QANo"] = $QANo;
			$_SESSION["TranNo"] = $TranNo;
			$_SESSION["Supplier"] = $Supplier;
			$_SESSION["Date"] = $Date;				
		}
	}
	else{
		$QANo = $_SESSION["QANo"];
		$Date = $_SESSION["Date"];
	}
	
	$TranNo = $_SESSION["TranNo"];	
	$Supplier = $_SESSION["Supplier"];
	
	// Product Select
	if(isset($_GET["materialcode"])){
		$Mat = $_GET["materialcode"];
		$sql = "select ProductCode,Name,MaterialType,Unit from Product where ProductCode='$Mat'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result) != 0){
			list($MaterialCode,$MaterialName,$MaterialType,$Unit) = mysql_fetch_row($result);
		}
	}
	
	// Keep Line Item
	if($_SESSION["LineItem"]){
		$LineItem = $_SESSION["LineItem"];
	}
	
	// Process Command
	if($_GET["cmd"]){
		if($_GET["cmd"] == "save" && ($_SESSION["TranNo"] == "")){	
			$isnotinputproduct = true;
		// save command
		}else if($_GET["cmd"] == "save"){
			// New
			if($mode == "new"){
				// generate running number
				$sql = "select LastNumber from runningnumber where RunTable='RQA'";
				$result = mysql_query($sql, $sqlconn);
				list($next_no) = mysql_fetch_row($result);
				
				$QANo = "RQA".$next_no;
				
				// increase next number
				$sql = "update runningnumber set LastNumber=LastNumber+1 where RunTable='RQA'";
				$result = mysql_query($sql, $sqlconn);
				
				// insert new header
				list($add_day,$add_month,$add_year) = split("/",$Date);
				$DateSql = "$add_year-$add_month-$add_day";
				$Chosen = $_SESSION["Chosen"];
				if($Chosen == "PO")
				{
					$sql = "insert into receivingqa(ReceivingQANo,Date,PurchaseNo) values ".
							"('$QANo','$DateSql','$TranNo')";
					$result = mysql_query($sql, $sqlconn);
				}
				else
				{
					$sql = "insert into receivingqa(ReceivingQANo,Date,WorkOrderNo) values ".
						   "('$QANo','$DateSql','$TranNo')";
					$result = mysql_query($sql, $sqlconn);
				}
				
				$mode = "edit";
			}
			// Old
			else{
				list($day,$month,$year) = split("/",$Date);
				$DateSql = "$year-$month-$day";
				
				$sql = "delete from  receivingqa where ReceivingQANo='$QANo'";
				$result = mysql_query($sql, $sqlconn);
				
				$Chosen = $_SESSION["Chosen"];
				if($Chosen == "PO")
				{
					$sql = "insert into receivingqa(ReceivingQANo,Date,PurchaseNo) values ".
							"('$QANo','$DateSql','$TranNo')";
					$result = mysql_query($sql, $sqlconn);
				}
				else
				{
					$sql = "insert into receivingqa(ReceivingQANo,Date,WorkOrderNo) values ".
						   "('$QANo','$DateSql','$TranNo')";
					$result = mysql_query($sql, $sqlconn);
				}
				
				// delete old lineitem
				$sql = "delete from receivingqalineitem where ReceivingQANo='$QANo'";
				$result = mysql_query($sql, $sqlconn);
			}
			
			// insert new lineitem	
			if(count($_SESSION["LineItem"]) > 0){
				foreach($_SESSION["LineItem"] as $row){
					$qa_product = $row["ProductCode"];
					$qa_totalqty = $row["TotalQuantity"];
					$qa_qtygood = $row["QuantityGood"];
					$qa_qtyreturn = $row["QuantityReturn"];
					$qa_remark = $row["Remarks"];
					
					$sql = "insert into receivingqalineitem values ('$QANo','$qa_product','$qa_totalqty'".				
							",'$qa_qtygood','$qa_qtyreturn','$qa_remark')";
					mysql_query($sql, $sqlconn);
				}
			}
		}
		// remove line item command
		else if($_GET["cmd"] == "removeline"){
			if(count($LineItem)>0){
				$removeat = $_GET["line"];
				foreach($LineItem as $row){
					if($removeat == $row["ProductCode"]){
						continue;
					}else{
						$tmp_LineItem[] = $row;
					}
				}
				$LineItem = $tmp_LineItem;
				$_SESSION["LineItem"] = $LineItem;
				unset($tmp_LineItem);
			}
		}
		
		// edit line item
		else if($_GET["cmd"] == "editline"){
			if(count($LineItem)>0){
				$editat = $_GET["line"];
				foreach($LineItem as $row){
					if($editat == $row["ProductCode"]){
						$MaterialCode = 	$row["ProductCode"];
						$MaterialName = 	$row["Name"];
						$MaterialType = 	$row["MaterialType"];
						$Unit = 			$row["Unit"];
						$MaterialTotal =	$row["TotalQuantity"];
						$MaterialGood =		$row["QuantityGood"];
						$Remarks  =			$row["Remarks"];
						$_SESSION["isEditLine"] = true;
						$iseditline = true;
						break;
					}
				}
			}
		}
	}
	
	// datatable session
	if($_POST["btn_add"]){
		// add new lineitem
		if($_POST["btn_add"] == "Add"){
			// check same appcode
			$already_data = false;
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["ProductCode"] == $_POST["txtMaterialCode"])
						$already_data = true;
				}
			}
			
			if(!$already_data){
				$new_lineitem = array(
					"ProductCode" => $_POST["txtMaterialCode"],
					"Name" => $_POST["txtMaterialName"],
					"MaterialType" => $_POST["txtMaterialType"],
					"TotalQuantity" => $_POST["txtMaterialTotal"],
					"QuantityGood" => $_POST["txtMaterialGood"],
					"QuantityReturn" => $_POST["txtMaterialTotal"] - $_POST["txtMaterialGood"],
					"Unit" => $_POST["txtUnit"],
					"Remarks" => $_POST["txtRemarks"],
				);
				
				$LineItem[] = $new_lineitem;
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$MaterialCode = "";
			$MaterialName = "";
			$MaterialType = "";
			$Unit = "";
			$Remarks = "";
			$MaterialTotal = "";
			$MaterialGood = "";
		}
		// edit line item
		else{
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["ProductCode"] == $_POST["txtMaterialCode"]){
						$new_lineitem = array(
							"ProductCode" => $_POST["txtMaterialCode"],
							"Name" => $_POST["txtMaterialName"],
							"MaterialType" => $_POST["txtMaterialType"],
							"TotalQuantity" => $_POST["txtMaterialTotal"],
							"QuantityGood" => $_POST["txtMaterialGood"],
							"QuantityReturn" => $_POST["txtMaterialTotal"] - $_POST["txtMaterialGood"],
							"Unit" => $_POST["txtUnit"],
							"Remarks" => $_POST["txtRemarks"],
						);
						$tmp_LineItem[] = $new_lineitem;
					}else{
						$tmp_LineItem[] = $row;
					}	
				}
				
				// fill session
				$LineItem = $tmp_LineItem;
				unset($tmp_LineItem);
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$_SESSION["isEditLine"] = false;
			$iseditline = false;
			
			$MaterialCode = "";
			$MaterialName = "";
			$MaterialType = "";
			$Unit = "";
			$Remarks = "";
			$MaterialTotal = "";
			$MaterialGood = "";
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receiving QA Number :: RQA</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

<?
	if($already_data){
		echo "alert(\"Already Data #".$_POST["txtMaterialCode"]."\");";
	}
	
	if($isnotinputproduct){
		echo "alert(\"Select PO/WO Number !\");";
	}
?>

var old_date = "";
var Interval;
var count;
// ajax function
function newXmlHttp(){
	var xmlhttp = false;
	
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlhttp = false;
		}
	}
	
	if(!xmlhttp && document.createElement){
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
function update_date(){
	old_date = document.getElementById('txtDate').value;
	Interval = setInterval('update()',500);
	count = 0;
}
function update(){
	var date = document.getElementById("txtDate").value;
	var url = "";
	count ++;
	if(count == 20){
		clearInterval(Interval);
		count = 0;
	}
	if(date != old_date){
		url = "update.php?date="+date;
		//alert(url);
		xmlhttp = newXmlHttp();
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		
		clearInterval(Interval);
		count = 0;
	}
}

function update_quantity(){
	var quantity = document.getElementById("txtQuantity").value;
	var url = "";
	url = "update.php?quantity="+quantity;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

function update_status(){
	var status = document.getElementById("txtStatus").value;
	var url = "";
	url = "update.php?status="+status;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	
	document.getElementById("txtPOWO").value = "";
	document.getElementById("txtStoreName").value = "";
}

// end function

function showtran_list() {
	window.open("select_transaction.php", "Transaction", "width=370 height=600");
}

function showmaterial_list() {
	window.open("select_material.php", "Material", "width=370 height=600");
}

function clear_app(){
	document.getElementById("txtMaterialCode").value = "";
	document.getElementById("txtMaterialName").value = "";
	document.getElementById("txtMaterialType").value = "";
	document.getElementById("txtMaterialGood").value = "";
	document.getElementById("txtMaterialTotal").value = "";
	document.getElementById("txtUnit").value = "";
	document.getElementById("txtRemarks").value = "";
}

function submit_header(){
	var tran = document.getElementById('txtPOWO').value;
	if(tran == ""){
		alert("Select PO/WO Number");
		return false;
	}else{
		document.location.href = "mo_edit.php?cmd=save";
		return true;
	}
}

function check_blank(){
	var materialcode = document.getElementById("txtMaterialCode").value;
	var total = document.getElementById("txtMaterialTotal").value;
	var good = document.getElementById("txtMaterialGood").value;
	var remarks = document.getElementById("txtRemarks").value;
	
	if(materialcode == ""){
		alert("Please choose material");
		return false;
	}
	if(total == ""){
		alert("Please input total quantity");
		return false;
	}
	if(good == ""){
		alert("Please input quantity good");
		return false;
	}
	if(remarks == ""){
		document.getElementById("txtRemarks").value = "-";
	}
	
	return true;
}

function delete_lineitem(matcode){
	var answer = confirm("Do you want to delete Material # " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=removeline&line="+matcode;
	}
	return false;
}
function edit_lineitem(matcode){
	var answer = confirm("Do you want to edit Material # " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=editline&line="+matcode;
	}
	return false;
}
function cancel_edit(){
	document.location.href = "mo_edit.php";
}
function calculate_total(){
	
	var qp = document.getElementById("txtMaterialQP").value;
	var q =  document.getElementById("txtQuantity").value;
	document.getElementById("txtMaterialTotal").value = (qp * q).toString();
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table width="160" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            <div align="center">
				<a href="#" onclick="submit_header();">
            	<img src="images/Save.png" width="48" height="48" border="0"/>                </a>            </div>            
              </td>
            <td>
            <div align="center">
            	<a href="mo_view.php">
                <img src="images/Cancel.png" width="48" height="48" border="0" />                </a>            </div>            </td>
            <td><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
          </tr>
          <tr>
            <td><div align="center">Save</div></td>
            <td><div align="center">Cancel</div></td>
            <td><div align="center">Report</div></td>
          </tr>
        </table>        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="50" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td height="25" colspan="2">Receiving QA No :</td>
            <td width="27%">
              <input name="txtTranNo" type="text" class="default_auto" id="txtTranNo" value="<?=$QANo;?>" readonly="readonly" /></td>
            <td width="12%"> Date : </td>
            <td width="44%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" onfocus="showCalendarControl(this);" onblur="update_date();" readonly="readonly"/></td>
         </tr>
          <tr>
            <td width="11%"><select name="txtStatus" class="default" id="txtStatus" onchange="update_status();">
              <option value="WO" <? if($Status == "WO") echo "selected=\"selected\"";?>>WO</option>
              <option value="PO" <? if($Status == "PO") echo "selected=\"selected\"";?>>PO</option>
            </select></td>
            <td width="6%" height="25">No :</td>
            <td>
              <input name="txtPOWO" type="text" class="default" id="txtPOWO" value="<?=$TranNo;?>" readonly="readonly" />
              <input name="button2" type="button" class="default_botton" id="button2" value="..." onclick="showtran_list();" /></td>
            <td>Supplier :</td>
            <td>
              <input name="txtStoreName" type="text" class="default_auto" id="txtStoreName" value="<?=$Supplier;?>" size="30" readonly="readonly" />              </td>
          </tr>
          </table>
          
        <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="4%" height="35">&nbsp;</td>
              <td width="4%">&nbsp;</td>
              <td width="8%" align="center">Matrial Code</td>
              <td width="20%" align="center">Name</td>
              <td width="10%" align="center">Type</td>
              <td width="10%" align="center">Total Quantity</td>
              <td width="10%" align="center"> Quantity Good</td>
              <td width="10%" align="center">Quantity Return</td>
              <td width="11%" align="center">Unit</td>
              <td width="12%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            if (count($LineItem)>0)
			{
				$index = 0;
				$style = "even";
				foreach($LineItem as $row)
				{
					$index ++;
					if($style == "even")  $style = "odd";
					else $style = "even";
			?>
            <tr class="<?=$style;?>">
              <td align="center" height="28"><img src="images/editpic.png" width="20" height="20" onclick="edit_lineitem('<?=$row["ProductCode"];?>');"/></td>
              <td align="center"><img src="images/delpic.png" width="20" height="20" onclick="delete_lineitem('<?=$row["ProductCode"];?>');"/></td>
              <td align="center"><?=$row["ProductCode"];?></td>
              <td><?=$row["Name"];?></td>
              <td><?=$row["MaterialType"];?></td>
              <td align="right"><?=$row["TotalQuantity"];?></td>
              <td align="right"><?=$row["QuantityGood"];?></td>
              <td align="right"><?=$row["QuantityReturn"];?></td>
              <td align="center"><?=$row["Unit"];?></td>
              <td><?=$row["Remarks"];?></td>
              <td>&nbsp;</td>
            </tr>
            <? } 
			} ?>
          </table>
          <br />
          <br />
          <form name="NewApp" method="post">
          <table border="0" width="96%" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <b>
                    <?
                    if(!$iseditline){
						echo "Add new material to list:"; 
						$btn_value = "Add";
					}else{
						echo "Edit material list:";
						$btn_value = "Edit";
					}
					?>
                    </b></td>
            </tr>
            <tr>
                <td>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr valign="bottom"> 
                        <td width="143" height="19">Material Code</td>   
                      <td width="149">Material Name</td>  
                      <td width="95">Type</td>  
                      <td width="73">Total Quantity</td>  
                      <td width="73">Quantity Good</td>  
                      <td width="66">Unit</td>   
                      <td>Remarks</td>                                  
                    </tr>
                    <tr>
                        <td height="34"><input name="txtMaterialCode" type="text" class="default" id="txtMaterialCode" value="<?=$MaterialCode;?>" size="15" readonly="readonly" />
                        <? if(!$iseditline){
                        echo "<input name=\"button3\" type=\"button\" class=\"default_botton\" id=\"button3\" value=\"...\" onclick=\"showmaterial_list();\" />";
                         } ?>                        </td>   
             			 <td>
                            <input name="txtMaterialName" type="text" class="default_auto" id="txtMaterialName" value="<?=$MaterialName;?>" size="25" readonly="readonly"/></td>  
                        <td>
                            <input name="txtMaterialType" type="text" class="default_auto" id="txtMaterialType" value="<?=$MaterialType;?>" size="15"  readonly="readonly"/></td>  
                        <td>
                            <input name="txtMaterialTotal" type="text" class="default" id="txtMaterialTotal" value="<?=$MaterialTotal;?>" size="8" onchange="calculate_total();"/></td>  
                        <td>
                            <input name="txtMaterialGood" type="text" class="default" id="txtMaterialGood" value="<?=$MaterialGood;?>" size="8"/></td>  
                        <td>
                            <input name="txtUnit" type="text" class="default_auto" id="txtUnit" value="<?=$Unit;?>" size="6" readonly="readonly"/></td> 
                        <td width="94">
                        <input name="txtRemarks" type="text" class="default" id="txtRemarks" value="<?=$Remarks;?>" size="15" /></td> 
                    </tr>
                </table>
                 <?
                    if($iseditline){
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Edit\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"cancel\" onclick=\"cancel_edit();\");\" />"; 
				 
					}else{
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Add\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"Clear\" onclick=\"clear_app();\" />"; 
					}
				 ?>                </td>
            </tr>
        </table>
        </form>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
