<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Show RQA report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript">
function select_qa(qa){
	window.location.href="report_show_qa.php?qano="+qa;
}
</script>
</head>
<body>

<?
	include "connect.inc.php";
	
	// if never select
	if(!isset($_GET['qano'])){
	
		if(!isset($_POST['txtsearch'])){
		$Search = "*";
		$sql = "select R.ReceivingQANo as QANo,R.Date,R.PurchaseNo,R.WorkOrderNo,P.PONo,P.Supplier as POSupplier,W.WONo,W.Supplier as WOSupplier ".
			   "from receivingqa R left join purchaseorder P on R.PurchaseNo = P.PONo left join  workorder W ".
			   "on R.WorkOrderNo = W.WONo";
		}else{
			$Search = $_POST['txtsearch'];
			if($Search == "*"){
				$sql = "select R.ReceivingQANo as QANo,R.Date,R.PurchaseNo,R.WorkOrderNo,P.PONo,P.Supplier as POSupplier,W.WONo,W.Supplier as WOSupplier ".
					   "from receivingqa R left join purchaseorder P on R.PurchaseNo = P.PONo left join  workorder W ".
					   "on R.WorkOrderNo = W.WONo";
			}else{
				$sql = "select R.ReceivingQANo as QANo,R.Date,R.PurchaseNo,R.WorkOrderNo,P.PONo,P.Supplier as POSupplier,W.WONo,W.Supplier as WOSupplier ".
					   "from receivingqa R left join purchaseorder P on R.PurchaseNo = P.PONo left join  workorder W ".
					   "on R.WorkOrderNo = W.WONo ".
						"where (R.ReceivingQANo like '%$Search%' or R.Date like '%$Search%' or P.PONo like '%$Search%' ".
						"or W.WONo like '%$Search%' or P.Supplier like '%$Search%' or W.Supplier like '%$Search%')";
			}
		}

	$result = mysql_query($sql);
?>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><div style="font-size:14px; font-weight:bold;">:: Select Receving QA Number  ::</div></td>
  </tr>
  <tr>
    <td>
    <br />
    <table width="400" border="0" cellpadding="0" cellspacing="0" class="border_color" align="center">
      <tr class="show_header">
        <td width="89" height="25">RQA Number</td>
        <td width="77">PO/WO No</td> 
        <td width="148">Supplier</td>
        <td width="82">Date</td>
      </tr>
      <?
      $round = 0;
	   while($data = mysql_fetch_assoc($result)){
		  if($round%2==0) echo "<tr class=\"even\">";
		  else echo "<tr class=\"odd\">";
		  
		  echo "<td height =\"22\" align=\"center\">&nbsp;<a href=\"javascript:select_qa('".$data["QANo"]."');\">".$data["QANo"]."</a></td>";
		  if($data["PONo"]=="")
		  {
		  	echo "<td align=\"center\">&nbsp;".$data["WONo"]."</a></td>";
		  	echo "<td align=\"center\">&nbsp;".$data["WOSupplier"]."</a></td>";
		  }
		  else
		  {
		  	echo "<td align=\"center\">&nbsp;".$data["PONo"]."</a></td>";
		    echo "<td align=\"center\">&nbsp;".$data["POSupplier"]."</a></td>";
		  }
		  echo "<td align=\"center\">&nbsp;".$data["Date"]."</a></td>";
		  
		  echo "</tr>";
		  $round ++;
		}
     ?>
      </table>
      <br />
        <form name="searchfrm" id="searchfrm" method="post" target="_self">
        <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><input name="txtsearch" type="text" class="default" id="txtsearch" value="<?=$Search;?>" size="60" /></td>
            <td><input name="button" type="submit" class="default_botton" id="button" value="Search" /></td>
          </tr>
        </table>
        </form>
        <br />  </td>
  </tr>
</table>
<?
	}
	// if choose mo already
	else
	{
	
	$get_qa = $_GET['qano'];
	
	$sql = "select ReceivingQANo as QANo,Date,PurchaseNo,WorkOrderNo from receivingqa where ReceivingQANo='$get_qa'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			list($QANo,$Date,$PurchaseNo,$WorkOrderNo) = mysql_fetch_row($result);
			
			if($PurchaseNo=="")
			{
				$sql = "select WONo,Supplier from workorder where WONo='$WorkOrderNo'";
				$result = mysql_query($sql, $sqlconn);
			}
			else
			{
				$sql = "select PONo,Supplier from purchaseorder where PONo='$PurchaseNo'";
				$result = mysql_query($sql, $sqlconn);
			}
			
			list($TranNo,$Supplier) = mysql_fetch_row($result);
			
			$Date=strftime("%d/%m/%Y",strtotime($Date));
		}
?>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center"><div style="font-size:14px; font-weight:bold;">:: Receiving QA Viewer  ::</div></td>
      </tr>
      <tr>
        <td><br />
          <table width="86%" height="50" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Receiving QA No :</td>
            <td width="30%"><?=$QANo;?></td>
            <td width="9%"> Date : </td>
            <td width="43%"><?=$Date;?></td>
          </tr>
          <tr>
            <td height="25">PO/WO No :</td>
            <td><?=$TranNo;?></td>
            <td>Supplier :</td>
            <td><?=$Supplier;?></td>
          </tr>
        </table>
          <br />
          
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="4%" height="32" align="center">&nbsp;#</td>
              <td width="9%" align="center">Material Code</td>
              <td width="19%">Name</td>
              <td width="9%" align="center">Type</td>
              <td width="12%" align="center">Total Quantity</td>
              <td width="13%" align="center">Quantity Good</td>
              <td width="12%" align="center">Quantity Return</td>
              <td width="6%" align="center">Unit</td>
              <td width="14%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            	$sql = "select M.MaterialCode,M.TotalQty,M.QtyGood,M.QtyReturn,M.Remark".
				       ",P.Name,P.MaterialType,P.Unit from receivingqalineitem M join Product P on M.MaterialCode = P.ProductCode ".
					   "where M.ReceivingQANo = '$QANo'";
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;<?=$count;?></td>
              <td align="center"><?=$data["MaterialCode"]?></td>
              <td>&nbsp;<?=$data["Name"]?></td>
              <td align="center"><?=$data["MaterialType"]?></td>
              <td align="right"><?=$data["TotalQty"]?></td>
              <td align="right"><?=$data["QtyGood"]?></td>
              <td align="right"><?=$data["QtyReturn"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              <td align="center"><?=$data["Remark"]?></td>
              <td align="center">&nbsp;</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<? } ?>

</body>
</html>