<?
	// index page
	session_start();
	include "connect.inc.php";
	session_unset();
	
	// delete transaction
	if($_GET["delete"]){
		$del_mo = $_GET["delete"];
		$sql = "select * from receivingqa where ReceivingQANo='$del_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$sql = "delete from receivingqalineitem where ReceivingQANo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			$sql = "delete from receivingqa where ReceivingQANo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			header("Location: mo_view.php");
		}
	}
	
	$get_mo = $_GET["qano"];
	
	if($get_mo!=""){
		$sql = "select ReceivingQANo,Date,PurchaseNo,WorkOrderNo from receivingqa where ReceivingQANo='$get_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$_SESSION["QANo"] = $get_mo;
			list($QANo,$Date,$PurchaseNo,$WorkOrderNo) = mysql_fetch_row($result);
			
			if($WorkOrderNo==""){
			$sql = "select PONo,Supplier from purchaseorder where PONo='$PurchaseNo'";
			$result = mysql_query($sql, $sqlconn);
			list($Status,$Supplier) = mysql_fetch_row($result);
			}
			else{
			$sql = "select WONo,Supplier from workorder where WONo='$WorkOrderNo'";
			$result = mysql_query($sql, $sqlconn);
			list($Status,$Supplier) = mysql_fetch_row($result);
			}
			$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			$menu_mode = "open";	
		}
	}else{
		$menu_mode = "new";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receiving QA :: RQA</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

function showqa_list() {
	window.open("select_qa.php", "QA", "width=320 height=600");
}

function delete_qa(qano){
	var answer = confirm("Do you want to delete QANo # " + qano);
	if(answer){
		document.location.href = "mo_view.php?delete="+qano;
	}
	return false;
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="mo_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <? if($menu_mode == "open"){ ?>
            <td width="50"><div align="center">
            	<a href="mo_edit.php?qano=<?=$_SESSION["QANo"]?>">
            		<img src="images/Modify.png" width="48" height="48" border="0" />
                </a>
            </div></td>
            <td width="50">
            <div align="center">
            	<a href="#">
       		    <img src="images/Delete.png" width="48" height="48" border="0" onclick="delete_qa('<?=$_SESSION["QANo"]?>');" />
                </a>
            </div></td>
            <? } ?>
            <td width="50"><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <? if($menu_mode == "open"){ ?>
            <td><div align="center">Edit</div></td>
            <td><div align="center">Delete</div></td>
            <? } ?>
            <td><div align="center">Report</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="50" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Receiving QA No :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default" id="txtTranNo" value="<?=$QANo;?>" readonly="readonly" />
              <input name="button" type="submit" class="default_botton" id="button" value="..." onclick="showqa_list();" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">PO/WO No :</td>
            <td><input name="txtStatus" type="text" class="default" id="txtStatus" value="<?=$Status;?>" size="20" readonly="readonly" /></td>
            <td>Supplier :</td>
            <td><input name="txtStatus2" type="text" class="default" id="txtStatus2" value="<?=$Supplier;?>" size="20"  readonly="readonly"/></td>
          </tr>
          </table>
          <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="4%" height="32" align="center">&nbsp;#</td>
              <td width="9%" align="center">Material Code</td>
              <td width="19%" align="center">Name</td>
              <td width="9%" align="center">Type</td>
              <td width="12%" align="center">Total Quantity</td>
              <td width="12%" align="center">Quantity Good</td>
              <td width="12%" align="center">Quantity Return</td>
              <td width="11%" align="center">Unit</td>
              <td width="10%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            	$sql = "select M.MaterialCode,M.TotalQty,M.QtyGood,M.QtyReturn,M.Remark".
				       ",P.Name,P.MaterialType,P.Unit from receivingqalineitem M join Product P on M.MaterialCode = P.ProductCode ".
					   "where M.ReceivingQANo = '$QANo'";
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;<?=$count;?></td>
              <td align="center"><?=$data["MaterialCode"]?></td>
              <td>&nbsp;<?=$data["Name"]?></td>
              <td><?=$data["MaterialType"]?></td>
              <td align="right"><?=$data["TotalQty"]?></td>
              <td align="right"><?=$data["QtyGood"]?></td>
              <td align="right"><?=$data["QtyReturn"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              <td><?=$data["Remark"]?></td>
              <td align="center">&nbsp;</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <? } ?>
          </table>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
