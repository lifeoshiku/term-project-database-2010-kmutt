<?
	include "connect.inc.php";
	session_unset();
	
	$sql = "select R.Date,L.MaterialCode,SUM(L.TotalQty) as SumTotayQty,SUM(L.QtyGood) as SumQtyGood".
			",SUM(L.QtyReturn) as SumQtyReturn,P.Name,P.Unit ".
			"from receivingqa R join receivingqalineitem L on R.ReceivingQANo = L.ReceivingQANo ".
			"join product P on L.MaterialCode = P.ProductCode";
	
	if(isset($_GET['year']) || isset($_GET['month'])){
		$sql =  $sql . " where ";
	}
		
	// add year and month
	if(isset($_GET['year'])){
		$sql = $sql." year(Date)=".$_GET['year'];
		$text = "of Year ".$_GET['year'];
		
		if(isset($_GET['month'])){
			$sql = $sql." and month(Date)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])."/".$_GET['year'];
		}
			
	}else{
	
		if(isset($_GET['month'])){
			$sql = $sql." month(Date)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])." in every year";
		}
	}
	// group by
	$sql = $sql." GROUP BY L.MaterialCode,P.Name,P.Unit";
	$res = mysql_query($sql,$sqlconn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RQA :: Sum of Product Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
</script>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center">
        <div style="font-size:14px; font-weight:bold;">:: Sum of Product Report ::</div>
        </td>
      </tr>
      <tr>
        <td><br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="18%" height="32">Material Code</td>
              <td width="22%" align="center">Material Name</td>
              <td width="15%" align="right">Total Quantity</td>
              <td width="15%" align="right">Quantity Good</td>
              <td width="15%" align="right">Quantity Return</td>
              <td width="15%" align="center">Unit</td>
              </tr>
            <?
				while($data = mysql_fetch_assoc($res)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28">&nbsp;<?=$data["MaterialCode"]?></td>
              <td><?=$data["Name"];?></td>
              <td align="right"><?=$data["SumTotayQty"];?></td>
              <td align="right"><?=$data["SumQtyGood"];?></td>
              <td align="right"><?=$data["SumQtyReturn"];?></td>
              <td align="center"><?=$data["Unit"];?></td>
              </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
