<?
	include "connect.inc.php";
	session_unset();
	$top = $_GET["material"];
	
	$sql = "SELECT flow.Date, F.FlowNo, SUM( F.Quantity ) AS Quantity, P.Unit ".
					"FROM flow JOIN flowlineitem F ON flow.FlowNo = F.FlowNo JOIN Product P ON F.MaterialCode = P.ProductCode".
					" where P.MaterialType = '$top' and flow.Reason = 'Use'";
			
	if(isset($_GET['year']) || isset($_GET['month'])){
		$sql =  $sql . " and ";
	}
		
	// add year and month
	if(isset($_GET['year'])){
		$sql = $sql." year(Date)=".$_GET['year'];
		$text = "of Year ".$_GET['year'];
			
		if(isset($_GET['month'])){
			$sql = $sql." and month(Date)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])."/".$_GET['year'];
		}
			
	}else{
	
		if(isset($_GET['month'])){
			$sql = $sql." month(Date)=".$_GET['month'];
			$text = "of ".num_to_month($_GET['month'])." in every year";
		}
	}
	
	// group by
	
	$sql = $sql." group by flow.Date, F.FlowNo";
	$res = mysql_query($sql,$sqlconn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Use :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(month != 0)
		url += "&month="+ month;
	if(year != 0)
		url += "&year="+ year;
		
	document.open(url,"Status","width=800 height=600");
}
</script>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center">
        <div style="font-size:14px; font-weight:bold;">:: Use <?=$_GET['top'];?> <?=$text;?>::</div>
        </td>
      </tr>
      <tr>
        <td><br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="27%" height="32" align="center">Date</td>
              <td width="39%">Flow No</td>
              <td width="10%">Quantity</td>
              <td width="24%" align="center">Unit</td>
              </tr>
            <?
				while($data = mysql_fetch_assoc($res)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center"><?=$data["Date"]?></td>
              <td>&nbsp;<?=$data["FlowNo"]?></td>
              <td align="right"><?=$data["Quantity"];?></td>
              <td align="center"><?=$data["Unit"];?></td>
              </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
