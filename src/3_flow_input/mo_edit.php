<?
	// index page
	session_start();
	include "connect.inc.php";
	
	// Check mode select
	if($_SESSION["Mode"]){
		$mode = $_SESSION["Mode"];
	}else{
		if(!isset($_GET["flowno"])){
			$mode = "new";
			$_SESSION['Reason'] = "Input";
			$_SESSION['Location'] = "None";
		}else{
			$mode = "edit";
		}
	}	
	
	// clear session
	if(isset($_SESSION["isEditLine"])){
		$iseditline = $_SESSION["isEditLine"];
		if($iseditline && $_GET["cmd"] == "editline"){
			$iseditline = true;
		}else{
			unset($_SESSION["isEditLine"]);
			$iseditline = false;
		}
	}else{
		$iseditline = false;
	}
	
	$_SESSION["Mode"] = $mode;
	
	// Start Program
	// if new Mode
	if($mode == "new"){
		$FlowNo = "NEW";
		if(isset($_SESSION["Date"]))
			$Date = $_SESSION["Date"];	
		else
			$Date = strftime("%d/%m/%Y",time());
	} 
	
	// if edit mode
	else if($mode == "edit" && !$_SESSION["isLoad"]) {
		// Get data from header
		if(isset($_GET["flowno"]))
			$get_transaction = $_GET["flowno"];
		else
			$get_transaction = $_SESSION["FlowNo"];
		
		$sql = "select FlowNo,Date,Location from flow where FlowNo='$get_transaction'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$Reason = "Input";
			list($FlowNo,$Date,$Location) = mysql_fetch_row($result);
			$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			// Read Line item
			$sql = "select F.MONo,F.MaterialCode,F.Quantity,F.Remarks".
				       ",P.Name,P.Unit from flowlineitem F join Product P on F.MaterialCode = P.ProductCode ".
					   "where F.FlowNo = '$FlowNo'";
					   
			$result = mysql_query($sql,$sqlconn);
			while($data = mysql_fetch_assoc($result)){
				$LineItem[] = $data;
			}
			$_SESSION["LineItem"] = $LineItem;
			$_SESSION["isLoad"] = 1;
			
			// Save Session
			$_SESSION["FlowNo"] = $FlowNo;
			$_SESSION["Reason"] = $Reason;
			$_SESSION["Date"] = $Date;	
			$_SESSION["Location"] = $Location;				
		}
	}
	else{
		$FlowNo = $_SESSION["FlowNo"];
		$Date = $_SESSION["Date"];
	}
		
	$Reason = $_SESSION["Reason"];
	$Location = $_SESSION["Location"];
	
	// Store Select
	if(isset($_GET["selmono"])){
		$Sel_MO = $_GET["selmono"];
	}
	
	// Product Select
	if(isset($_GET["materialcode"])){
		$Mat = $_GET["materialcode"];
		$sql = "select ProductCode,Name,Unit from Product where ProductCode='$Mat'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result) != 0){
			list($Sel_MaterialCode,$Sel_MaterialName,$Unit) = mysql_fetch_row($result);
		}
	}
	
	// Keep Line Item
	if($_SESSION["LineItem"]){
		$LineItem = $_SESSION["LineItem"];
	}
	
	// Process Command
	if($_GET["cmd"]){
		if($_GET["cmd"] == "save" && ($_SESSION["Location"] == "None")){	
			$isnotinputproduct = true;
		// save command
		}else if($_GET["cmd"] == "save"){
			// New
			if($mode == "new"){
				// generate running number
				$sql = "select LastNumber from runningnumber where RunTable='INP'";
				$result = mysql_query($sql, $sqlconn);
				list($next_no) = mysql_fetch_row($result);
				
				$FlowNo = "INP".$next_no;
				
				// increase next number
				$sql = "update runningnumber set LastNumber=LastNumber+1 where RunTable='INP'";
				$result = mysql_query($sql, $sqlconn);
				
				// insert new header
				list($add_day,$add_month,$add_year) = split("/",$Date);
				$DateSql = "$add_year-$add_month-$add_day";
				$sql = "insert into flow values ".
						"('$FlowNo','$DateSql','$Reason','$Location')";
				$result = mysql_query($sql, $sqlconn);
				
				$mode = "edit";
			}
			// Old
			else{
				list($day,$month,$year) = split("/",$Date);
				$DateSql = "$year-$month-$day";
				$sql = "update flow set Date='$DateSql',Location='$Location' where ".
						"FlowNo='$FlowNo'";
				$result = mysql_query($sql, $sqlconn);
				
				// delete old lineitem
				$sql = "delete from flowlineitem where FlowNo='$FlowNo'";
				$result = mysql_query($sql, $sqlconn);
			}
			
			// insert new lineitem	
			if(count($_SESSION["LineItem"]) > 0){
				foreach($_SESSION["LineItem"] as $row){
					$save_mo = $row["MONo"];
					$save_mc = $row["MaterialCode"];
					$save_qu = $row["Quantity"];
					$save_remark = $row["Remarks"];
					
					$sql = "insert into flowlineitem values ('$FlowNo','$save_mo','$save_mc'".				
							",'$save_qu','$save_remark')";
					//echo $sql;
					mysql_query($sql, $sqlconn);
				}
			}
		}
		// remove line item command
		else if($_GET["cmd"] == "removeline"){
			if(count($LineItem)>0){
				$removeat = $_GET["line"];
				$removeat2 = $_GET["line2"];
				foreach($LineItem as $row){
					if($removeat == $row["MaterialCode"] && $removeat2 == $row["MONo"]){
						continue;
					}else{
						$tmp_LineItem[] = $row;
					}
				}
				$LineItem = $tmp_LineItem;
				$_SESSION["LineItem"] = $LineItem;
				unset($tmp_LineItem);
			}
		}
		
		// edit line item
		else if($_GET["cmd"] == "editline"){
			if(count($LineItem)>0){
				$editat = $_GET["line"];
				$editat2 = $_GET["line2"];
				foreach($LineItem as $row){
					if($editat == $row["MaterialCode"] && $editat2 == $row["MONo"]){
						$Sel_MO = 	$row["MONo"];
						$Sel_MaterialCode = 	$row["MaterialCode"];
						$Sel_MaterialName = 	$row["Name"];
						$Unit = 			$row["Unit"];
						$Sel_Quantity =		$row["Quantity"];
						$Remarks  =			$row["Remarks"];
						$_SESSION["isEditLine"] = true;
						$iseditline = true;
						break;
					}
				}
			}
		}
	}
	
	// datatable session
	if($_POST["btn_add"]){
		// add new lineitem
		if($_POST["btn_add"] == "Add"){
			// check same appcode
			$already_data = false;
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["MONo"] == $_POST["txtMONo"] && $row["MaterialCode"] == $_POST["txtMaterialCode"])
						$already_data = true;
				}
			}
			
			if(!$already_data){
				$new_lineitem = array(
					"MONo" => $_POST["txtMONo"],
					"MaterialCode" => $_POST["txtMaterialCode"],
					"Name" => $_POST["txtMaterialName"],
					"Quantity" => $_POST["txtQuantity"],
					"Unit" => $_POST["txtUnit"],
					"Remarks" => $_POST["txtRemarks"],
				);
				
				$LineItem[] = $new_lineitem;
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$Sel_MO = "";
			$Sel_MaterialCode = "";
			$Sel_MaterialName = "";
			$Unit = "";
			$Sel_Quantity ="";
			$Remarks = "";

		}
		// edit line item
		else{
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["MaterialCode"] == $_POST["txtMaterialCode"] && $row["MONo"] == $_POST["txtMONo"]){
						$new_lineitem = array(
							"MONo" => $_POST["txtMONo"],
							"MaterialCode" => $_POST["txtMaterialCode"],
							"Name" => $_POST["txtMaterialName"],
							"Quantity" => $_POST["txtQuantity"],
							"Unit" => $_POST["txtUnit"],
							"Remarks" => $_POST["txtRemarks"],
						);
						$tmp_LineItem[] = $new_lineitem;
					}else{
						$tmp_LineItem[] = $row;
					}	
				}
				
				// fill session
				$LineItem = $tmp_LineItem;
				unset($tmp_LineItem);
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$_SESSION["isEditLine"] = false;
			$iseditline = false;
			
			$Sel_MO = "";
			$Sel_MaterialCode = "";
			$Sel_MaterialName = "";
			$Unit = "";
			$Sel_Quantity ="";
			$Remarks = "";
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: Goods Move in Line (Input)</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

<?
	if($already_data){
		echo "alert(\"Already Data\");";
	}
	
	if($isnotinputproduct){
		echo "alert(\"Select Location !\");";
	}
?>

var old_date = "";
var Interval;
var count;
// ajax function
function newXmlHttp(){
	var xmlhttp = false;
	
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlhttp = false;
		}
	}
	
	if(!xmlhttp && document.createElement){
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
function update_date(){
	old_date = document.getElementById('txtDate').value;
	Interval = setInterval('update()',500);
	count = 0;
}
function update(){
	var date = document.getElementById("txtDate").value;
	var url = "";
	count ++;
	if(count == 20){
		clearInterval(Interval);
		count = 0;
	}
	if(date != old_date){
		url = "update.php?date="+date;
		//alert(url);
		xmlhttp = newXmlHttp();
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		
		clearInterval(Interval);
		count = 0;
	}
}

function update_location(){
	var local = document.getElementById("txtLocation").value;
	var url = "";
	url = "update.php?location="+local;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

// end function

function showmo_list() {
	window.open("select_mo.php", "MO", "width=320 height=600");
}

function showmaterial_list(mo) {
	window.open("select_material.php?mono="+mo, "Material", "width=370 height=600");
}

function clear_app(){
	document.getElementById("txtMONo").value = "";
	document.getElementById("txtMaterialCode").value = "";
	document.getElementById("txtMaterialName").value = "";
	document.getElementById("txtQuantity").value = "";
	document.getElementById("txtUnit").value = "";
	document.getElementById("txtRemarks").value = "";
}

function submit_header(){
	var location = document.getElementById('txtLocation').value;
	if(location == "None"){
		alert("Select Location");
		return false;
	}else{
		document.location.href = "mo_edit.php?cmd=save";
		return true;
	}
}

function check_blank(){
	var mono = document.getElementById("txtMONo").value;
	var mc = document.getElementById("txtMaterialCode").value;
	var qt = document.getElementById("txtQuantity").value;
	var remarks = document.getElementById("txtRemarks").value;
	
	if(mono == ""){
		alert("Please choose MONO");
		return false;
	}
	if(mc == ""){
		alert("Please input Material Code");
		return false;
	}
	if(qt == ""){
		alert("Please input quantity");
		return false;
	}
	if(remarks == ""){
		document.getElementById("txtRemarks").value = "-";
	}
	
	return true;
}

function delete_lineitem(mo,matcode){
	alert(mo + "" + matcode);
	var answer = confirm("Do you want to delete MO # "+mo+" at # " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=removeline&line="+matcode+"&line2="+mo;
	}
	return false;
}
function edit_lineitem(mo,matcode){
	var answer = confirm("Do you want to edit MO # "+mo+" at # " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=editline&line="+matcode+"&line2="+mo;
	}
	return false;
}
function cancel_edit(){
	document.location.href = "mo_edit.php";
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table width="160" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            <div align="center">
				<a href="#" onclick="submit_header();">
            	<img src="images/Save.png" width="48" height="48" border="0"/>
                </a>            
               </div>            
              </td>
            <td>
            <div align="center">
            	<a href="mo_view.php">
                <img src="images/Cancel.png" width="48" height="48" border="0" />                </a>            </div>            </td>
            <td><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
          </tr>
          <tr>
            <td><div align="center">Save</div></td>
            <td><div align="center">Cancel</div></td>
            <td><div align="center">Report</div></td>
          </tr>
        </table>        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="50" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">MO Number :</td>
            <td width="26%">
              <input name="txtFlowNo" type="text" class="default_auto" id="txtFlowNo" value="<?=$FlowNo;?>" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" onfocus="showCalendarControl(this);" onblur="update_date();" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">Reason :</td>
            <td>
              <input name="txtReason" type="text" class="default_auto" id="txtReason" value="<?=$Reason;?>" readonly="readonly" /></td>
            <td>Location :</td>
            <td><select name="txtLocation" class="default" id="txtLocation" onchange="update_location();">
              <option value="None" <? if($Location == "None") echo "selected=\"selected\"";?>>None</option>
				<?	
			  		$sql = "select * from location";
			  		$res = mysql_query($sql,$sqlconn);
					while($data = mysql_fetch_assoc($res)){
						echo "<option value=\"".$data["Location"]."\"";
						if($Location == $data["Location"])
							echo "selected=\"selected\"";
						echo ">".$data["LocationName"]."</option>";
					} 
				?>
            </select></td>
          </tr>
          </table>
          
        <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="5%" height="35">&nbsp;</td>
              <td width="5%">&nbsp;</td>
              <td width="13%" align="center">MO No</td>
              <td width="10%" align="center">Material Code</td>
              <td width="22%" align="center">Name</td>
              <td width="11%" align="center">Quantity</td>
              <td width="8%" align="center">Unit</td>
              <td width="24%" align="center">Remarks</td>
              <td width="2%">&nbsp;</td>
            </tr>
            <?
            if (count($LineItem)>0)
			{
				$index = 0;
				$style = "even";
				foreach($LineItem as $row)
				{
					$index ++;
					if($style == "even")  $style = "odd";
					else $style = "even";
			?>
            <tr class="<?=$style;?>">
              <td align="center" height="28"><img src="images/editpic.png" width="20" height="20" onclick="edit_lineitem('<?=$row["MONo"];?>','<?=$row["MaterialCode"];?>');"/></td>
              <td align="center"><img src="images/delpic.png" width="20" height="20" onclick="delete_lineitem('<?=$row["MONo"];?>','<?=$row["MaterialCode"];?>');"/></td>
              <td align="center"><?=$row["MONo"]?></td>
              <td align="center">&nbsp;<?=$row["MaterialCode"]?></td>
              <td><?=$row["Name"]?></td>
              <td align="right"><?=$row["Quantity"]?></td>
              <td align="center"><?=$row["Unit"]?></td>
              <td align="center"><?=$row["Remarks"]?></td>
              <td>&nbsp;</td>
            </tr>
            <? } 
			} ?>
          </table>
          <br />
          <br />
          <form name="NewApp" method="post">
          <table border="0" width="96%" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <b>
                    <?
                    if(!$iseditline){
						echo "Add new list:"; 
						$btn_value = "Add";
					}else{
						echo "Edit list:";
						$btn_value = "Edit";
					}
					?>
                    </b></td>
            </tr>
            <tr>
                <td>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr valign="bottom"> 
                        <td width="149" height="19">MO No</td>   
                      <td width="141">Material Code</td>  
                      <td width="149">Material Name</td>  
                      <td width="66">Quantity</td>  
                      <td width="64">Unit</td>   
                      <td>Remarks</td>                                  
                    </tr>
                    <tr>
                        <td height="34"><input name="txtMONo" type="text" class="default" id="txtMONo" value="<?=$Sel_MO;?>" size="15" readonly="readonly" />
                        <? if(!$iseditline){
                        echo "<input name=\"button3\" type=\"button\" class=\"default_botton\" id=\"button3\" value=\"...\" onclick=\"showmo_list();\" />";
                         } ?>                        </td>   
             			 <td>
                            <input name="txtMaterialCode" type="text" class="default" id="txtMaterialCode" value="<?=$Sel_MaterialCode;?>" size="15"/>
                            <? if(!$iseditline){ ?>
                            <input name="select_mat_mo" type="button" class="default_botton" id="select_mat_mo" value="..." onclick="showmaterial_list('<?=$Sel_MO;?>')" /></td>  
							<? }?>
                        <td>
                            <input name="txtMaterialName" type="text" class="default_auto" id="txtMaterialName" value="<?=$Sel_MaterialName;?>" size="25"  readonly="readonly"/></td>  
                        <td>
                            <input name="txtQuantity" type="text" class="default" id="txtQuantity" value="<?=$Sel_Quantity;?>" size="6"/></td>  
                        <td>
                            <input name="txtUnit" type="text" class="default_auto" id="txtUnit" value="<?=$Unit;?>" size="6" readonly="readonly"/></td> 
                        <td width="199">
                        <input name="txtRemarks" type="text" class="default" id="txtRemarks" value="<?=$Remarks;?>" size="30" /></td> 
                    </tr>
                </table>
                 <?
                    if($iseditline){
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Edit\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"cancel\" onclick=\"cancel_edit();\");\" />"; 
				 
					}else{
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Add\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"Clear\" onclick=\"clear_app();\" />"; 
					}
				 ?>                </td>
            </tr>
        </table>
        </form>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
