<?
	// index page
	session_start();
	include "connect.inc.php";
	
	// Check mode select
	if($_SESSION["Mode"]){
		$mode = $_SESSION["Mode"];
	}else{
		if(!isset($_GET["qano"])){
			$mode = "new";
			
		}else{
			$mode = "edit";
		}
	}	
	
	// clear session
	if(isset($_SESSION["isEditLine"])){
		$iseditline = $_SESSION["isEditLine"];
		if($iseditline && $_GET["cmd"] == "editline"){
			$iseditline = true;
		}else{
			unset($_SESSION["isEditLine"]);
			$iseditline = false;
		}
	}else{
		$iseditline = false;
	}
	
	$_SESSION["Mode"] = $mode;
	
	// Store Select
	//if(isset($_GET["productcode"]))
	//{
	//	$ProductCode = $_GET["productcode"];
	//	$sql = "select ProductCode,Name from Product where ProductCode='$ProductCode'";
	//	$result = mysql_query($sql, $sqlconn);
	//	if(mysql_num_rows($result) != 0)
	//	{
	//		list($ProductCode,$Name) = mysql_fetch_row($result);
	//		$_SESSION["ProductCode"] = $ProductCode;
	//		$_SESSION["Name"] = $Name;
	//	}
	//}
	
	// Start Program
	// if new Mode
	
	if($mode == "new")
	{
		$QANo = "NEW";
		if(isset($_SESSION["Date"]))
			$Date = $_SESSION["Date"];
		else
			$Date = strftime("%d/%m/%Y",time());
	} 
	
	// if edit mode
	else if($mode == "edit" && !$_SESSION["isLoad"]) 
	{
		// Get data from header
		if(isset($_GET["qano"]))
			$get_transaction = $_GET["qano"];
		else
			$get_transaction = $_SESSION["QANo"];
		
		$sql = "select QANo,Date from QA where QANo='$get_transaction'";
		$result = mysql_query($sql, $sqlconn);
		
		if(mysql_num_rows($result))
		{
			list($QANo,$Date) = mysql_fetch_row($result);
			$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			// Get Product data
			//$sql = "select ProductCode,Name from Product where ProductCode='$ProductCode'";
			//$result = mysql_query($sql, $sqlconn);
			//list($ProductCode,$Name) = mysql_fetch_row($result);
			
			// Read Line item
			//$sql = "select M.ProductCode,M.QuantityPer,M.TotalQuantity,M.Remarks".
			//	       ",P.Name,P.MaterialType,P.Unit from MOLineItem M join Product P on M.ProductCode = P.ProductCode ".
			//		   "where M.MONo = '$MONo'";
					   
			$sql = " SELECT L.MONo,M.ProductCode,P.Name,M.Quantity,L.Good,L.Hold,L.Damage,L.Remark".
			" FROM QALineItem L JOIN MOrder M on L.MONo = M.MONo ".
			" INNER JOIN Product P ON P.ProductCode = M.ProductCode".
			" WHERE L.QANo = '$QANo'";
					   
			$result = mysql_query($sql,$sqlconn);
			while($data = mysql_fetch_assoc($result)){
				$data["Date"] = strftime("%d/%m/%Y",strtotime($data["Date"]));
				$LineItem[] = $data;
			}
			
			$_SESSION["LineItem"] = $LineItem;
			$_SESSION["isLoad"] = 1;
			

			// Save Session
			$_SESSION["QANo"] = $QANo;
			$_SESSION["Date"] = $Date;				
		}
	}
	else
	{
		$QANo = $_SESSION["QANo"];
		$Date = $_SESSION["Date"];
	}
	
	$QANo = $_SESSION["QANo"];
	//$Date = $_SESSION["Date"];
	
	// Product Select
	if(isset($_GET["mono"])){
		$Mat = $_GET["mono"];
		$sql = "select M.MONo,P.ProductCode,P.Name,M.Quantity ".
		"from MOrder M JOIN Product P ON M.ProductCode = P.ProductCode where MONo = '$Mat';";
		
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result) != 0){
			list($MONo,$ProductCode,$ProductName,$QuantityTotal) = mysql_fetch_row($result);
		}
	}
	
	// Keep Line Item
	if($_SESSION["LineItem"]){
		$LineItem = $_SESSION["LineItem"];
	}
	
	// Process Command
	if($_GET["cmd"]){
	//	if($_GET["cmd"] == "save" && ($_SESSION["ProductCode"] == ""))
	//	{	
	//		$isnotinputproduct = true;
	//	// save command
	//	}
		if($_GET["cmd"] == "save")
		{
			// New
			if($mode == "new")
			{
				// generate running number
				$sql = "select LastNumber from runningnumber where RunTable='QA'";
				$result = mysql_query($sql, $sqlconn);
				list($next_no) = mysql_fetch_row($result);
				
				$QANo = "QA".$next_no;
				
				// increase next number
				$sql = "update runningnumber set LastNumber=LastNumber+1 where RunTable='QA'";
				$result = mysql_query($sql, $sqlconn);
				
				// insert new header
				list($add_day,$add_month,$add_year) = split("/",$Date);
				$DateSql = "$add_year-$add_month-$add_day";
				$sql = "insert into QA values ".
						"('$QANo','$DateSql')";
				$result = mysql_query($sql, $sqlconn);
				
				$mode = "edit";
			}
			// Old
			else
			{
				list($day,$month,$year) = split("/",$Date);
				$DateSql = "$year-$month-$day";
				$sql = "update QA set QANo='$QANo', Date='$DateSql' where QANo='$QANo'";
				$result = mysql_query($sql, $sqlconn);
				
				// delete old lineitem
				$sql = "delete from QALineItem where QANo='$QANo'";
				$result = mysql_query($sql, $sqlconn);
			}
			
			// insert new lineitem
			$countL = 1;	
			if(count($_SESSION["LineItem"]) > 0)
			{
				foreach($_SESSION["LineItem"] as $row)
				{			
					$mo_c = $countL;
					$mo_mn = $row["MONo"];
					$mo_g = $row["Good"];
					$mo_h = $row["Hold"];
					$mo_d = $row["Damage"];
					$mo_remark = $row["Remark"];
					
					$sql = "insert into QALineItem values ('$mo_c','$QANo','$mo_mn','$mo_g'".				
							",'$mo_h','$mo_d','$mo_remark')";
					mysql_query($sql, $sqlconn);
					
					$countL++;
				}
			}
		}
		
		// remove line item command
		else if($_GET["cmd"] == "removeline"){
			if(count($LineItem)>0){
				$removeat = $_GET["line"];
				foreach($LineItem as $row){
					if($removeat == $row["MONo"]){
						continue;
					}else{
						$tmp_LineItem[] = $row;
					}
				}
				$LineItem = $tmp_LineItem;
				$_SESSION["LineItem"] = $LineItem;
				unset($tmp_LineItem);
			}
		}
		
		// edit line item
		else if($_GET["cmd"] == "editline"){
			if(count($LineItem)>0){
				$editat = $_GET["line"];
				foreach($LineItem as $row){
					if($editat == $row["MONo"]){
						$MONo			 	= 	$row["MONo"];
						$ProductCode 		= 	$row["ProductCode"];
						$ProductName		= 	$row["Name"];
						$QuantityTotal 		= 	$row["Quantity"];
						$Good 				=	$row["Good"];
						$Hold 				=	$row["Hold"];
						$Damage 			=	$row["Damage"];
						$Remarks  			=	$row["Remark"];
						$_SESSION["isEditLine"] = true;
						$iseditline = true;
						break;
					}
				}
			}
		}
	}
	
	// datatable session
	if($_POST["btn_add"]){
		// add new lineitem
		if($_POST["btn_add"] == "Add")
		{
			// check same appcode
			$already_data = false;
			if(count($LineItem) > 0)
			{
				foreach($LineItem as $row)
				{
					if($row["MONo"] == $_POST["txtMONo"])
						$already_data = true;
				}
			}
			
			if(!$already_data)
			{
				$new_lineitem = array(
					"MONo" => $_POST["txtMONo"],
					"ProductCode" => $_POST["txtProductCode"],
					"Name" => $_POST["txtProductName"],
					"Quantity" => $_POST["txtQuantityTotal"],
					"Good" => $_POST["txtGood"],
					"Hold" => $_POST["txtHold"],
					"Damage" => $_POST["txtDamage"],
					"Remark" => $_POST["txtRemarks"],
				);
				
				$LineItem[] = $new_lineitem;
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$MONo = "";
			$ProductCode = "";
			$ProductName = "";
			$QuantityTotal = "";
			$Good ="";
			$Hold = "";
			$Damage = "";
			$Remark = "";
		}
		// edit line item
		else{
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["MONo"] == $_POST["txtMONo"]){
						$new_lineitem = array(
							"MONo" => $_POST["txtMONo"],
							"ProductCode" => $_POST["txtProductCode"],
							"Name" => $_POST["txtProductName"],
							"Quantity" => $_POST["txtQuantityTotal"],
							"Good" => $_POST["txtGood"],
							"Hold" => $_POST["txtHold"],
							"Damage" => $_POST["txtDamage"],
							"Remark" => $_POST["txtRemarks"],
						);
						$tmp_LineItem[] = $new_lineitem;
					}else{
						$tmp_LineItem[] = $row;
					}	
				}
				
				// fill session
				$LineItem = $tmp_LineItem;
				unset($tmp_LineItem);
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$_SESSION["isEditLine"] = false;
			$iseditline = false;
			
			$MONo = "";
			$ProductCode = "";
			$ProductName = "";
			$QuantityTotal = "";
			$Good ="";
			$Hold = "";
			$Damage = "";
			$Remarks = "";
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: MO</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

<?
	if($already_data){
		echo "alert(\"Already Data #".$_POST["txtMaterialCode"]."\");";
	}
	
	if($isnotinputproduct){
		echo "alert(\"Select Store !\");";
	}
?>

var old_date = "";
var Interval;
var count;
// ajax function
function newXmlHttp(){
	var xmlhttp = false;
	
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlhttp = false;
		}
	}
	
	if(!xmlhttp && document.createElement){
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
function update_date(){
	old_date = document.getElementById('txtDate').value;
	Interval = setInterval('update()',500);
	count = 0;
}
function update(){
	var date = document.getElementById("txtDate").value;
	var url = "";
	count ++;
	if(count == 20){
		clearInterval(Interval);
		count = 0;
	}
	if(date != old_date){
		url = "update.php?date="+date;
		//alert(url);
		xmlhttp = newXmlHttp();
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		
		clearInterval(Interval);
		count = 0;
	}
}

function update_quantity(){
	var quantity = document.getElementById("txtQuantity").value;
	var url = "";
	url = "update.php?quantity="+quantity;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

function update_status(){
	var status = document.getElementById("txtStatus").value;
	var url = "";
	url = "update.php?status="+status;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

// end function

function showproduct_list() {
	window.open("select_product.php", "Product", "width=370 height=600");
}

function showmono_list() {
	window.open("select_mono.php", "MONo", "width=370 height=600");
}

function clear_app(){
	document.getElementById("txtMaterialCode").value = "";
	document.getElementById("txtMaterialName").value = "";
	document.getElementById("txtMaterialType").value = "";
	document.getElementById("txtMaterialQP").value = "";
	document.getElementById("txtMaterialTotal").value = "";
	document.getElementById("txtUnit").value = "";
	document.getElementById("txtRemarks").value = "";
}

function check_blank(){
	var materialcode = document.getElementById("txtMaterialCode").value;
	var total = document.getElementById("txtMaterialTotal").value;
	var qp = document.getElementById("txtMaterialQP").value;
	var remarks = document.getElementById("txtRemarks").value;
	
	if(materialcode == ""){
		alert("Please choose material");
		return false;
	}
	if(total == ""){
		alert("Please input total");
		return false;
	}
	if(qp == ""){
		alert("Please input quantity");
		return false;
	}
	if(remarks == ""){
		document.getElementById("txtRemarks").value = "-";
	}
	
	return true;
}

function delete_lineitem(matcode){
	var answer = confirm("Do you want to delete MO Number # " + matcode);
	if(answer){
		document.location.href = "qa_edit.php?cmd=removeline&line="+matcode;
	}
	return false;
}
function edit_lineitem(matcode){
	var answer = confirm("Do you want to edit MO Number # " + matcode);
	if(answer){
		document.location.href = "qa_edit.php?cmd=editline&line="+matcode;
	}
	return false;
}
function cancel_edit(){
	document.location.href = "mo_edit.php";
}
function calculate_total(){
	
	var qp = document.getElementById("txtMaterialQP").value;
	var q =  document.getElementById("txtQuantity").value;
	document.getElementById("txtMaterialTotal").value = (qp * q).toString();
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table width="160" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            <div align="center">
				<a href="qa_edit.php?cmd=save">
            	<img src="images/Save.png" width="48" height="48" border="0"/>                </a>            </div>            
              </td>
            <td>
            <div align="center">
            	<a href="qa_view.php">
                <img src="images/Cancel.png" width="48" height="48" border="0" />                </a>            </div>            </td>
            <td><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
          </tr>
          <tr>
            <td><div align="center">Save</div></td>
            <td><div align="center">Cancel</div></td>
            <td><div align="center">Report</div></td>
          </tr>
        </table>        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="100" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">QA Number :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default_auto" id="txtTranNo" value="<?=$QANo;?>" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" onfocus="showCalendarControl(this);" onblur="update_date();" readonly="readonly"/></td>
         </tr>
          
          <tr>
            <td height="25">&nbsp;</td>
            
            <td colspan="3"><label></label></td>
          </tr>
          </table>
          
        <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="4%" height="35">&nbsp;</td>
              <td width="4%">&nbsp;</td>
              <td width="11%" align="center">MO No</td>
              <td width="9%" align="center">Product Code</td>
              <td width="18%" align="center">Product Name</td>
              <td width="12%" align="center">Quantity Total</td>
              <td width="9%" align="center">Good</td>
              <td width="8%" align="center">Hold</td>
              <td width="8%" align="center">Damage</td>
              <td width="15%" align="center">Remarks</td>
              <td width="2%">&nbsp;</td>
            </tr>
            <?
            if (count($LineItem)>0)
			{
				$index = 0;
				$style = "even";
				foreach($LineItem as $row)
				{
					$index ++;
					if($style == "even")  $style = "odd";
					else $style = "even";
			?>
            <tr class="<?=$style;?>">
              <td align="center" height="28"><img src="images/editpic.png" width="20" height="20" onclick="edit_lineitem('<?=$row["MONo"];?>');"/></td>
              <td align="center"><img src="images/delpic.png" width="20" height="20" onclick="delete_lineitem('<?=$row["MONo"];?>');"/></td>
              <td align="center"><?=$row["MONo"];?></td>
              <td align="center"><?=$row["ProductCode"];?></td>
              <td align="center"><?=$row["Name"];?></td>
              <td align="right"><?=$row["Quantity"];?></td>
              <td align="right"><?=$row["Good"];?></td>
              <td align="right"><?=$row["Hold"];?></td>
              <td align="center"><?=$row["Damage"];?></td>
              <td align="center"><?=$row["Remark"];?></td>
              <td>&nbsp;</td>
            </tr>
            <? } 
			} ?>
          </table>
          <br />
          <br />
          <form name="NewApp" method="post">
          <table border="0" width="96%" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <b>
                    <?
                    if(!$iseditline){
						echo "Add new material to list:"; 
						$btn_value = "Add";
					}else{
						echo "Edit material list:";
						$btn_value = "Edit";
					}
					?>
                    </b></td>
            </tr>
            <tr>
                <td>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr valign="bottom"> 
                        <td width="146" height="19"><div align="center">MO No</div></td>   
                      <td width="87"><div align="center">Product Code</div></td>  
                      <td width="156"><div align="center">Product Name</div></td>  
                      <td width="95"><div align="right">Quantity Total</div></td>  
                      <td width="57"><div align="right">Good</div></td>  
                      <td width="52"><div align="right">Hold</div></td>
                      <td width="54"><div align="right">Damage</div></td>   
                      <td><div align="center">Remarks</div></td>                                  
                    </tr>
                    <tr>
                        <td height="34"><div align="center">
                          <input name="txtMONo" type="text" class="default" id="txtMONo" value="<?=$MONo;?>" size="15" readonly="readonly" />
                          <? if(!$iseditline){
                        echo "<input name=\"button3\" type=\"button\" class=\"default_botton\" id=\"button3\" value=\"...\" onclick=\"showmono_list();\" />";
                         } ?>                        
                        </div></td>   
             			 <td>
                            <div align="center">
                              <input name="txtProductCode" type="text" class="default_auto" id="txtProductCode" value="<?=$ProductCode;?>" size="10" readonly="readonly"/>
                            </div></td>  
                        <td>
                            <div align="center">
                              <input name="txtProductName" type="text" class="default_auto" id="txtProductName" value="<?=$ProductName;?>" size="25"  readonly="readonly"/>
                            </div></td>  
                        <td><div align="right">
                          <input name="txtQuantityTotal" type="text" class="default_auto" id="txtQuantityTotal" value="<?=$QuantityTotal;?>" size="14"  readonly="readonly"/>
                        </div></td>  
                        <td>
                            <div align="right">
                              <input name="txtGood" type="text" class="default" id="txtGood" value="<?=$Good;?>" size="5"/>
                            </div></td>  
                        <td><div align="right">
                          <input name="txtHold" type="text" class="default" id="txtHold" value="<?=$Hold;?>" size="5"/>
                        </div></td>
                        <td>
                            <div align="right">
                              <input name="txtDamage" type="text" class="default" id="txtDamage" value="<?=$Damage;?>" size="5"/>
                            </div></td> 
                        <td width="121">
                          <div align="center">
                            <input name="txtRemarks" type="text" class="default" id="txtRemarks" value="<?=$Remarks;?>" size="16" />
                        </div></td> 
                    </tr>
                </table>
                 <?
                    if($iseditline){
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Edit\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"cancel\" onclick=\"cancel_edit();\");\" />"; 
				 
					}else{
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Add\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"Clear\" onclick=\"clear_app();\" />"; 
					}
				 ?>                </td>
            </tr>
        </table>
        </form>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
