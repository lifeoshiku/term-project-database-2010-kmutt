<?
	// index page
	session_start();
	include "connect.inc.php";
	session_unset();
	
	// delete transaction
	if($_GET["delete"]){
		$del_mo = $_GET["delete"];
		$sql = "select * from QA where QANo='$del_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$sql = "delete from QALineItem where QANo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			$sql = "delete from QA where QANo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			header("Location: qa_view.php");
		}
	}
	
	$get_qa = $_GET["qano"];
	
	if($get_qa!=""){
		$sql = "select * from QA where QANo='$get_qa'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$_SESSION["QANo"] = $get_qa;
			list($QANo,$Date) = mysql_fetch_row($result);
			
			//$sql = "select ProductCode,Name from product where ProductCode='$ProductCode'";
			//$result = mysql_query($sql, $sqlconn);
			//list($ProductCode,$Name) = mysql_fetch_row($result);
			
			//$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			$menu_mode = "open";	
		}
	}else{
		$menu_mode = "new";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: MO</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

function showqa_list() {
	window.open("select_qa.php", "MO", "width=320 height=600");
}

function delete_qa(mono){
	var answer = confirm("Do you want to delete QANo # " + mono);
	if(answer){
		document.location.href = "qa_view.php?delete="+mono;
	}
	return false;
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="qa_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <? if($menu_mode == "open"){ ?>
            <td width="50"><div align="center">
            	<a href="qa_edit.php?qano=<?=$_SESSION["QANo"]?>">
            		<img src="images/Modify.png" width="48" height="48" border="0" />
                </a>
            </div></td>
            <td width="50">
            <div align="center">
            	<a href="#">
       		    <img src="images/Delete.png" width="48" height="48" border="0" onclick="delete_qa('<?=$_SESSION["QANo"]?>');" />
                </a>
            </div></td>
            <? } ?>
            <td width="50"><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <? if($menu_mode == "open"){ ?>
            <td><div align="center">Edit</div></td>
            <td><div align="center">Delete</div></td>
            <? } ?>
            <td><div align="center">Report</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="100" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">QA Number :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default" id="txtTranNo" value="<?=$QANo;?>" readonly="readonly" />
              <input name="button" type="submit" class="default_botton" id="button" value="..." onclick="showqa_list();" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          </table>
          <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="4%" height="32" align="center">&nbsp;#</td>
              <td width="8%" align="center">MO No</td>
              <td width="12%" align="center">Product Code</td>
              <td width="19%" align="center">Product Name</td>
              <td width="13%" align="right">Quantity Total</td>
              <td width="10%" align="right">Good</td>
              <td width="10%" align="right">Hold</td>
              <td width="9%" align="right">Damage</td>
              <td width="14%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
			
			//$sql = 	"select QALineItem.ItemNo,MOrder.MONo,Product.ProductCode,Product.Name ".
			//		",MOrder.Quantity,QALineItem.Good,QALineItem.Hold,QALineItem.Damage".
			//		"from MOrder INNER JOIN" .
			//		"QALineItem ON MOrder.MONo = QALineItem.MONo INNER JOIN" .
			//		"QA ON QALineItem.QANo = QA.QANo INNER JOIN" .
			//		"Product ON MOrder.ProductCode = Product.ProductCode";


            	$sql = " SELECT L.MONo,M.ProductCode,P.Name,M.Quantity,L.Good,L.Hold,L.Damage,L.Remark".
				       " FROM QALineItem L JOIN MOrder M on L.MONo = M.MONo ".
					   " INNER JOIN Product P ON P.ProductCode = M.ProductCode".
					   " WHERE L.QANo = '$QANo'";
					   
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;<?=$count;?></td>
              <td align="center"><?=$data["MONo"]?></td>
              <td align="center">&nbsp;<?=$data["ProductCode"]?></td>
              <td align="center"><?=$data["Name"]?></td>
              <td align="right"><?=$data["Quantity"]?></td>
              <td align="right"><?=$data["Good"]?></td>
              <td align="right"><?=$data["Hold"]?></td>
              <td align="right"><?=$data["Damage"]?></td>
              <td align="center"><?=$data["Remark"]?></td>
              <td>&nbsp;</td>
            </tr>
            <? } ?>
          </table>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
