<?
	include "connect.inc.php";
	session_unset();
	$top = $_GET['top'];
	
	$sql = "SELECT MOrder.ProductCode AS ProductCode,Product.Name AS Name,SUM(MOrder.Quantity) AS Total,SUM(QALineItem.Hold) AS Hold , SUM(QALineItem.Hold)
/ MOrder.Quantity AS Ratio
FROM QA INNER JOIN
QALineItem ON QA.QANo = QALineItem.QANo INNER JOIN
MOrder ON QALineItem.MONo = MOrder.MONo INNER JOIN
Product ON MOrder.ProductCode = Product.ProductCode
GROUP BY MOrder.ProductCode,Product.Name,MOrder.Quantity,QALineItem.Hold
ORDER BY Ratio DESC,MOrder.ProductCode
LIMIT 0,$top";
			
	//if(isset($_GET['year']) || isset($_GET['month'])){
	//	$sql =  $sql . " where ";
	//}
		
	// add year and month
	//if(isset($_GET['year'])){
	//	$sql = $sql." year(Date)=".$_GET['year'];
	//	$text = "of Year ".$_GET['year'];
		
	//	if(isset($_GET['month'])){
	//		$sql = $sql." and month(Date)=".$_GET['month'];
	//		$text = "of ".num_to_month($_GET['month'])."/".$_GET['year'];
	//	}
			
	//}else{
	//
	//	if(isset($_GET['month'])){
	//		$sql = $sql." month(Date)=".$_GET['month'];
	//		$text = "of ".num_to_month($_GET['month'])." in every year";
	//	}
	//}
	
	// group by
	//$sql = $sql." group by Product.ProductCode, Product.Name order by Quantity desc limit 0,".$top;
	$res = mysql_query($sql,$sqlconn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MO :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(month != 0)
		url += "&month="+ month;
	if(year != 0)
		url += "&year="+ year;
		
	document.open(url,"Status","width=800 height=600");
}
</script>
</head>

<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center">
        <div style="font-size:14px; font-weight:bold;">:: Product TOP Hold <?=$_GET['top'];?>
            Hold::</div>
        </td>
      </tr>
      <tr>
        <td><br />
          <table width="74%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="10%" align="center">No</td>
              <td width="15%" height="32" align="center">Product Code</td>
              <td width="29%"><div align="center">Product Name</div></td>
              <td width="16%" align="center"><div align="right">Quantity Total</div></td>
              <td width="17%" align="center"><div align="right">Product Hold</div></td>
              <td width="13%" align="center"><div align="right">Ratio</div></td>
              </tr>
            <?
				while($data = mysql_fetch_assoc($res))
				{
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td align="center"><?=$count?></td>
              <td height="28" align="center"><?=$data["ProductCode"]?></td>
              <td align="center">&nbsp;<?=$data["Name"]?></td>
              <td align="right"><?=$data["Total"];?></td>
              <td align="right"><?=$data["Hold"];?></td>
              <td align="right"><?=$data["Ratio"];?></td>
              </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
