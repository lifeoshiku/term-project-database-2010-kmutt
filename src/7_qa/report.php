<?
	session_start();
	include "connect.inc.php";
	session_unset();
	
	$sql = "CREATE VIEW findyear AS SELECT year( Date ) AS year FROM MOrder GROUP BY year; ";
	mysql_query($sql,$sqlconn);
	
	$sql = "SELECT min( year ) as minyear , max( year ) as maxyear FROM findyear";
	$res = mysql_query($sql,$sqlconn);
	list($min,$max) = mysql_fetch_row($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MO :: View Report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript" type="text/javascript">
function show_select_mo(){
	document.open("report_show_qa.php","QA","width=850 height=600");
}

function show_select_pd(){
	document.open("report_show_pd.php","PD","width=850 height=600");
}

function show_production_status(prme){
	var url = "report_show_prst.php?type="+prme;
	var month = document.getElementById("month_b").value;
	var year = document.getElementById("year_b").value;
	
	if(prme != "all"){
		if(month != 0)
			url += "&month="+ month;
		if(year != 0)
			url += "&year="+ year;
	}
		
	document.open(url,"Status","width=850 height=600");
}

function show_product_of_month(){
	var url = "report_show_tophold.php";
	//var month = document.getElementById("month_c").value;
	//var year = document.getElementById("year_c").value;
	
	var top = prompt("Please enter number of rank. Ex. 10 for Top 10 Hold.","5");

	if(top == "" || top == null || isNaN(top)){
		alert("Please input number ^^!");
	}else{
		// add tail
		url += "?top="+top;
			
		//if(month != 0)
		//	url += "&month="+ month;
		//if(year != 0)
		//	url += "&year="+ year;
		
		document.open(url,"Tophold","width=850 height=600");
	}
}
</script></head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="qa_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <td width="50">
            <div align="center"><a href="qa_view.php"><img src="images/Modify.png" width="48" height="48" border="0" /></a></div>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <td><div align="center">Open</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td><br />
          <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
            <tr>
              <td class="show_header">View QA Report ::</td>
            </tr>
            <tr>
              <td>
              <br />
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr valign="top">
                  <td align="center" valign="top"><b>QA Report</b></td>
                  <td align="center" valign="top" style="border-left:dotted 1px #333333;"><b>Report by Product</b></td>
                  <td align="center" valign="top" style="border-left:dotted 1px #333333;"><strong>Top N Hold Report</strong></td>
                </tr>
                <tr valign="top">
                  <td width="33%" height="41" align="center" valign="top"><input name="button4" type="button" class="default_botton" id="button4" value="Select QA" onclick="show_select_mo();" /></td>
                  <td width="33%" align="center" valign="top" style="border-left:dotted 1px #333333;">                  <input name="button" type="button" class="default_botton" id="button" value="Select Product" onclick="show_select_pd();" />
                    <br /></td>
                  <td width="34%" align="center" valign="top" style="border-left:dotted 1px #333333;"><input name="button5" type="button" class="default_botton" id="button5" value="Show Report" onclick="show_product_of_month();" /></td>
                </tr>
              </table>
              </td>
            </tr>
            
            <tr>
              <td height="16">&nbsp;</td>
            </tr>
          </table>
          <br />          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
