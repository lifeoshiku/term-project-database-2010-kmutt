<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Show MO report</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language="javascript">
function select_mo(mo){
	window.location.href="report_show_mo.php?mono="+mo;
}
</script>
</head>
<body>

<?
	include "connect.inc.php";
	
	// if never select
	if(!isset($_GET['mono'])){
	
		if(!isset($_POST['txtsearch'])){
		$Search = "*";
		$sql = "select M.FormulaNo,M.Date,M.FormulaName,P.Name from formula M ". 
		       "join Product P on M.ProductCode = P.ProductCode limit 0,20";
		}else{
			$Search = $_POST['txtsearch'];
			if($Search == "*"){
				$sql = "select M.FormulaNo,M.Date,M.FormulaName,P.Name from formula M ".
				       "join Product P on M.ProductCode = P.ProductCode limit 0,20";
			}else{
				$sql = "select M.FormulaNo,M.Date,M.FormulaName,P.Name ". 
				       "from formula M join Product P on M.ProductCode = P.ProductCode ".
					   "where (M.FormulaNo like '%$Search%' or M.Date like '%$Search%' or M.ProductCode like '%$Search%'".
					   " or P.Name like '%$Search%') limit 0,20";
			}
		}

	$result = mysql_query($sql);
?>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><div style="font-size:14px; font-weight:bold;">:: Select Formula Number  ::</div></td>
  </tr>
  <tr>
    <td>
    <br />
    <table width="400" border="0" cellpadding="0" cellspacing="0" class="border_color" align="center">
      <tr class="show_header">
        <td width="89" height="25">Formula Number</td>
        <td width="77">Formula Name</td> 
        <td width="148">Product Name</td>
        <td width="82">Date</td>
      </tr>
      <?
      $round = 0;
	   while($data = mysql_fetch_assoc($result)){
		  if($round%2==0) echo "<tr class=\"even\">";
		  else echo "<tr class=\"odd\">";
		  
		  echo "<td height =\"22\" align=\"center\">&nbsp;<a href=\"javascript:select_mo('".$data["FormulaNo"]."');\">".$data["FormulaNo"]."</a></td>";
		  echo "<td align=\"center\">&nbsp;".$data["FormulaName"]."</a></td>";
		  echo "<td align=\"center\">&nbsp;".$data["Name"]."</a></td>";
		  echo "<td align=\"center\">&nbsp;".$data["Date"]."</a></td>";
		  
		  echo "</tr>";
		  $round ++;
		}
     ?>
      </table>
      <br />
        <form name="searchfrm" id="searchfrm" method="post" target="_self">
        <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><input name="txtsearch" type="text" class="default" id="txtsearch" value="<?=$Search;?>" size="60" /></td>
            <td><input name="button" type="submit" class="default_botton" id="button" value="Search" /></td>
          </tr>
        </table>
        </form>
        <br />  </td>
  </tr>
</table>
<?
	}
	// if choose mo already
	else
	{
	
	$get_mo = $_GET['mono'];
	
	$sql = "select F.FormulaNo,F.FormulaName,F.Date,P.Name from ".
		   "formula F join product P on F.ProductCode = P.ProductCode where FormulaNo='$get_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			list($MONo,$Name,$Date,$Status) = mysql_fetch_row($result);
			
			$Date=strftime("%d/%m/%Y",strtotime($Date));
		}
?>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" align="center"><div style="font-size:14px; font-weight:bold;">:: Formula Viewer  ::</div></td>
      </tr>
      <tr>
        <td><br />
          <table width="86%" height="75" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Formula Number :</td>
            <td width="30%"><?=$MONo;?></td>
            <td width="9%"> Date : </td>
            <td width="43%"><?=$Date;?></td>
          </tr>
          <tr>
            <td height="25">Formula Name :</td>
            <td>&nbsp;<?=$Name;?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>

          <tr>
            <td height="25">Product Name  : </td>
            <td colspan="3"><?=$Status;?></td>
          </tr>
        </table>
          <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="5%" height="32" align="center">&nbsp;#</td>
              <td width="10%" align="center">Material Code</td>
              <td width="21%" align="center">Name</td>
              <td width="10%" align="center">Type</td>
              <td width="12%" align="center">Quantity</td>
              <td width="7%" align="center">Unit</td>
              <td width="19%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            	$sql = "select M.ProductCode,M.Quantity,M.Remarks".
				       ",P.Name,P.MaterialType,P.Unit from formulalineitem M join Product P on M.ProductCode = P.ProductCode ".
					   "where M.FormulaNo = '$MONo'";
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;
                  <?=$count;?></td>
              <td align="center"><?=$data["ProductCode"]?></td>
              <td>&nbsp;
                  <?=$data["Name"]?></td>
              <td align="center"><?=$data["MaterialType"]?></td>
              <td align="right"><?=$data["Quantity"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              <td align="center"><?=$data["Remarks"]?></td>
              <td>&nbsp;</td>
            </tr>
            <? } ?>
          </table>
          <br />
          <br /></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<? } ?>

</body>
</html>