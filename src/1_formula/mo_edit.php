<?
	// index page
	session_start();
	include "connect.inc.php";
	
	// Check mode select
	if($_SESSION["Mode"]){
		$mode = $_SESSION["Mode"];
	}else{
		if(!isset($_GET["mono"])){
			$mode = "new";
			$_SESSION['Status'] = "Process";
		}else{
			$mode = "edit";
		}
	}	
	
	// clear session
	if(isset($_SESSION["isEditLine"])){
		$iseditline = $_SESSION["isEditLine"];
		if($iseditline && $_GET["cmd"] == "editline"){
			$iseditline = true;
		}else{
			unset($_SESSION["isEditLine"]);
			$iseditline = false;
		}
	}else{
		$iseditline = false;
	}
	
	$_SESSION["Mode"] = $mode;
	
	// Store Select
	if(isset($_GET["productcode"])){
		$ProductCode = $_GET["productcode"];
		$sql = "select ProductCode,Name from Product where ProductCode='$ProductCode'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result) != 0){
			list($ProductCode,$Name) = mysql_fetch_row($result);
			$_SESSION["ProductCode"] = $ProductCode;
			$_SESSION["Name"] = $Name;
		}
	}
	
	// Start Program
	// if new Mode
	if($mode == "new"){
		$MONo = "NEW";
		if(isset($_SESSION["Date"]))
			$Date = $_SESSION["Date"];	
		else
			$Date = strftime("%d/%m/%Y",time());
	} 
	// if edit mode
	else if($mode == "edit" && !$_SESSION["isLoad"]) {
		// Get data from header
		if(isset($_GET["mono"]))
			$get_transaction = $_GET["mono"];
		else
			$get_transaction = $_SESSION["MONo"];
		
		$sql = "select FormulaNo,FormulaName,Date,ProductCode from formula where FormulaNo='$get_transaction'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			list($MONo,$Quantity,$Date,$ProductCode) = mysql_fetch_row($result);
			$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			// Get Product data
			$sql = "select ProductCode,Name from Product where ProductCode='$ProductCode'";
			$result = mysql_query($sql, $sqlconn);
			list($ProductCode,$Name) = mysql_fetch_row($result);
			
			// Read Line item
			$sql = "select F.ProductCode,F.Quantity as QuantityPer,F.Remarks".
				       ",P.Name,P.MaterialType,P.Unit from formulalineitem F join Product P on F.ProductCode = P.ProductCode ".
					   "where F.FormulaNo = '$MONo'";
			$result = mysql_query($sql,$sqlconn);
			while($data = mysql_fetch_assoc($result)){
				$data["Date"] = strftime("%d/%m/%Y",strtotime($data["Date"]));
				$LineItem[] = $data;
			}
			$_SESSION["LineItem"] = $LineItem;
			$_SESSION["isLoad"] = 1;
			
			// Save Session
			$_SESSION["MONo"] = $MONo;
			$_SESSION["ProductCode"] = $ProductCode;
			$_SESSION["Name"] = $Name;
			$_SESSION["Quantity"] = $Quantity;
			$_SESSION["Date"] = $Date;				
		}
	}
	else{
		$MONo = $_SESSION["MONo"];
		$Date = $_SESSION["Date"];
	}
	
	$ProductCode = $_SESSION["ProductCode"];	
	$Name = $_SESSION["Name"];
	$Quantity = $_SESSION["Quantity"];
	
	// Product Select
	if(isset($_GET["materialcode"])){
		$Mat = $_GET["materialcode"];
		$sql = "select ProductCode,Name,MaterialType,Unit from Product where ProductCode='$Mat'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result) != 0){
			list($MaterialCode,$MaterialName,$MaterialType,$Unit) = mysql_fetch_row($result);
		}
	}
	
	// Keep Line Item
	if($_SESSION["LineItem"]){
		$LineItem = $_SESSION["LineItem"];
	}
	
	// Process Command
	if($_GET["cmd"]){
		if($_GET["cmd"] == "save" && ($_SESSION["ProductCode"] == "")){	
			$isnotinputproduct = true;
		// save command
		}else if($_GET["cmd"] == "save"){
			// New
			if($mode == "new"){
				// generate running number
				$sql = "select LastNumber from runningnumber where RunTable='FM'";
				$result = mysql_query($sql, $sqlconn);
				list($next_no) = mysql_fetch_row($result);
				
				$MONo = "FM".$next_no;
				
				// increase next number
				$sql = "update runningnumber set LastNumber=LastNumber+1 where RunTable='FM'";
				$result = mysql_query($sql, $sqlconn);
				
				// insert new header
				list($add_day,$add_month,$add_year) = split("/",$Date);
				$DateSql = "$add_year-$add_month-$add_day";
				$sql = "insert into formula values ".
						"('$MONo','$Quantity','$DateSql','$ProductCode')";
				$result = mysql_query($sql, $sqlconn);
				
				$mode = "edit";
			}
			// Old
			else{
				list($day,$month,$year) = split("/",$Date);
				$DateSql = "$year-$month-$day";
				$sql = "update formula set FormulaName='$Quantity',ProductCode='$ProductCode',".
						"Date='$DateSql' where FormulaNo='$MONo'";
				
				//echo $sql;
				$result = mysql_query($sql, $sqlconn);
				
				// delete old lineitem
				$sql = "delete from formulalineitem where FormulaNo='$MONo'";
				$result = mysql_query($sql, $sqlconn);
			}
			
			// insert new lineitem	
			if(count($_SESSION["LineItem"]) > 0){
				foreach($_SESSION["LineItem"] as $row){
					$mo_pc = $row["ProductCode"];
					$mo_qp = $row["QuantityPer"];
					$mo_remark = $row["Remarks"];
					
					$sql = "insert into formulalineitem values ('$MONo','$mo_pc','$mo_qp','$mo_remark')";
					mysql_query($sql, $sqlconn);
				}
			}
		}
		// remove line item command
		else if($_GET["cmd"] == "removeline"){
			if(count($LineItem)>0){
				$removeat = $_GET["line"];
				foreach($LineItem as $row){
					if($removeat == $row["ProductCode"]){
						continue;
					}else{
						$tmp_LineItem[] = $row;
					}
				}
				$LineItem = $tmp_LineItem;
				$_SESSION["LineItem"] = $LineItem;
				unset($tmp_LineItem);
			}
		}
		
		// edit line item
		else if($_GET["cmd"] == "editline"){
			if(count($LineItem)>0){
				$editat = $_GET["line"];
				foreach($LineItem as $row){
					if($editat == $row["ProductCode"]){
						$MaterialCode = 	$row["ProductCode"];
						$MaterialName = 	$row["Name"];
						$MaterialType = 	$row["MaterialType"];
						$Unit = 			$row["Unit"];
						$MaterialQP =		$row["QuantityPer"];
						$Remarks  =			$row["Remarks"];
						$_SESSION["isEditLine"] = true;
						$iseditline = true;
						break;
					}
				}
			}
		}
	}
	
	// datatable session
	if($_POST["btn_add"]){
		// add new lineitem
		if($_POST["btn_add"] == "Add"){
			// check same appcode
			$already_data = false;
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["ProductCode"] == $_POST["txtMaterialCode"])
						$already_data = true;
				}
			}
			
			if(!$already_data){
				$new_lineitem = array(
					"ProductCode" => $_POST["txtMaterialCode"],
					"Name" => $_POST["txtMaterialName"],
					"MaterialType" => $_POST["txtMaterialType"],
					"QuantityPer" => $_POST["txtMaterialQP"],
					"Unit" => $_POST["txtUnit"],
					"Remarks" => $_POST["txtRemarks"],
				);
				
				$LineItem[] = $new_lineitem;
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$MaterialCode = "";
			$MaterialName = "";
			$MaterialType = "";
			$Unit = "";
			$MaterialQP ="";
			$Remarks = "";
			$MaterialTotal = "";
		}
		// edit line item
		else{
			if(count($LineItem) > 0){
				foreach($LineItem as $row){
					if($row["ProductCode"] == $_POST["txtMaterialCode"]){
						$new_lineitem = array(
							"ProductCode" => $_POST["txtMaterialCode"],
							"Name" => $_POST["txtMaterialName"],
							"MaterialType" => $_POST["txtMaterialType"],
							"QuantityPer" => $_POST["txtMaterialQP"],
							"Unit" => $_POST["txtUnit"],
							"Remarks" => $_POST["txtRemarks"],
						);
						$tmp_LineItem[] = $new_lineitem;
					}else{
						$tmp_LineItem[] = $row;
					}	
				}
				
				// fill session
				$LineItem = $tmp_LineItem;
				unset($tmp_LineItem);
				$_SESSION["LineItem"] = $LineItem;
			}
			
			$_SESSION["isEditLine"] = false;
			$iseditline = false;
			
			$MaterialCode = "";
			$MaterialName = "";
			$MaterialType = "";
			$Unit = "";
			$MaterialQP ="";
			$Remarks = "";
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: Formula</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

<?
	if($already_data){
		echo "alert(\"Already Data #".$_POST["txtMaterialCode"]."\");";
	}
	
	if($isnotinputproduct){
		echo "alert(\"Select Product !\");";
	}
?>

var old_date = "";
var Interval;
var count;
// ajax function
function newXmlHttp(){
	var xmlhttp = false;
	
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){
			xmlhttp = false;
		}
	}
	
	if(!xmlhttp && document.createElement){
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
function update_date(){
	old_date = document.getElementById('txtDate').value;
	Interval = setInterval('update()',500);
	count = 0;
}
function update(){
	var date = document.getElementById("txtDate").value;
	var url = "";
	count ++;
	if(count == 20){
		clearInterval(Interval);
		count = 0;
	}
	if(date != old_date){
		url = "update.php?date="+date;
		//alert(url);
		xmlhttp = newXmlHttp();
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		
		clearInterval(Interval);
		count = 0;
	}
}

function update_quantity(){
	var quantity = document.getElementById("txtQuantity").value;
	var url = "";
		url = "update.php?quantity="+quantity;
		xmlhttp = newXmlHttp();
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		document.location.href = "mo_edit.php";
}

function update_status(){
	var status = document.getElementById("txtStatus").value;
	var url = "";
	url = "update.php?status="+status;
	xmlhttp = newXmlHttp();
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

// end function

function showformula_list() {
	var product = document.getElementById("txtStoreCode").value;
	if(product == ""){
		alert("Please select Product");
	}else{
		window.open("select_formula.php?product="+product, "formula", "width=420 height=600");
	}
}

function showproduct_list() {
	window.open("select_product.php", "Product", "width=370 height=600");
}

function showmaterial_list() {
	window.open("select_material.php", "Material", "width=370 height=600");
}

function clear_app(){
	document.getElementById("txtMaterialCode").value = "";
	document.getElementById("txtMaterialName").value = "";
	document.getElementById("txtMaterialType").value = "";
	document.getElementById("txtMaterialQP").value = "";
	document.getElementById("txtMaterialTotal").value = "";
	document.getElementById("txtUnit").value = "";
	document.getElementById("txtRemarks").value = "";
}

function submit_header(){
	var store = document.getElementById('txtStoreCode').value;
	var name = document.getElementById('txtQuantity').value;
	
	if(store == ""){
		alert("Select Product");
		return false;
	}else if(name == ""){
		alert("Input Name");
		return false;
	}else{
		document.location.href = "mo_edit.php?cmd=save";
		return true;
	}
}

function check_blank(){
	var materialcode = document.getElementById("txtMaterialCode").value;
	var qp = document.getElementById("txtMaterialQP").value;
	var remarks = document.getElementById("txtRemarks").value;
	
	if(materialcode == ""){
		alert("Please choose material");
		return false;
	}
	if(qp == ""){
		alert("Please input quantity");
		return false;
	}
	if(remarks == ""){
		document.getElementById("txtRemarks").value = "-";
	}
	
	return true;
}

function delete_lineitem(matcode){
	var answer = confirm("Do you want to delete Material # " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=removeline&line="+matcode;
	}
	return false;
}
function edit_lineitem(matcode){
	var answer = confirm("Do you want to edit Material # " + matcode);
	if(answer){
		document.location.href = "mo_edit.php?cmd=editline&line="+matcode;
	}
	return false;
}
function cancel_edit(){
	document.location.href = "mo_edit.php";
}
function calculate_total(){
	
	var qp = document.getElementById("txtMaterialQP").value;
	var q =  document.getElementById("txtQuantity").value;
	document.getElementById("txtMaterialTotal").value = (qp * q).toString();
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table width="160" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            <div align="center">
				<a href="#" onclick="submit_header();">
            	<img src="images/Save.png" width="48" height="48" border="0"/>                </a>            </div>            
              </td>
            <td>
            <div align="center">
            	<a href="mo_view.php">
                <img src="images/Cancel.png" width="48" height="48" border="0" />                </a>            </div>            </td>
            <td><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
          </tr>
          <tr>
            <td><div align="center">Save</div></td>
            <td><div align="center">Cancel</div></td>
            <td><div align="center">Report</div></td>
          </tr>
        </table>        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="75" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Formula Number :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default_auto" id="txtTranNo" value="<?=$MONo;?>" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" onfocus="showCalendarControl(this);" onblur="update_date();" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">Formula Name :</td>
            <td colspan="3"><input name="txtQuantity" type="text" class="default" id="txtQuantity" value="<?=$Quantity;?>" size="50" onchange="update_quantity();"/></td>
          </tr>
          <tr>
            <td height="25">Product :</td>
            <td>
              <input name="txtStoreCode" type="text" class="default" id="txtStoreCode" value="<?=$ProductCode;?>" readonly="readonly" />
              <input name="button2" type="button" class="default_botton" id="button2" value="..." onclick="showproduct_list();" /></td>
            <td>Product  Name :</td>
            <td>
              <input name="txtStoreName" type="text" class="default_auto" id="txtStoreName" value="<?=$Name;?>" size="40" readonly="readonly" />              </td>
          </tr>
         
          </table>
          
        <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="5%" height="35">&nbsp;</td>
              <td width="5%">&nbsp;</td>
              <td width="8%" align="center">Matrial Code</td>
              <td width="20%">Name</td>
              <td width="10%" align="center">Type</td>
              <td width="11%" align="center">Quantity</td>
              <td width="7%" align="center">Unit</td>
              <td width="20%" align="center">Remarks</td>
              <td width="1%">&nbsp;</td>
            </tr>
            <?
            if (count($LineItem)>0)
			{
				$index = 0;
				$style = "even";
				foreach($LineItem as $row)
				{
					$index ++;
					if($style == "even")  $style = "odd";
					else $style = "even";
			?>
            <tr class="<?=$style;?>">
              <td align="center" height="28"><img src="images/editpic.png" width="20" height="20" onclick="edit_lineitem('<?=$row["ProductCode"];?>');"/></td>
              <td align="center"><img src="images/delpic.png" width="20" height="20" onclick="delete_lineitem('<?=$row["ProductCode"];?>');"/></td>
              <td align="center"><?=$row["ProductCode"];?></td>
              <td><?=$row["Name"];?></td>
              <td align="center"><?=$row["MaterialType"];?></td>
              <td align="right"><?=$row["QuantityPer"];?></td>
              <td align="right"><?=$row["Unit"];?></td>
              <td align="center"><?=$row["Remarks"];?></td>
              <td>&nbsp;</td>
            </tr>
            <? } 
			} ?>
          </table>
          <br />
          <br />
          <form name="NewApp" method="post">
          <table border="0" width="96%" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <b>
                    <?
                    if(!$iseditline){
						echo "Add new material to list:"; 
						$btn_value = "Add";
					}else{
						echo "Edit material list:";
						$btn_value = "Edit";
					}
					?>
                    </b></td>
            </tr>
            <tr>
                <td>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr valign="bottom"> 
                        <td width="144" height="19">Material Code</td>   
                      <td width="157">Material Name</td>  
                      <td width="102">Type</td>  
                      <td width="61">Quantity</td>  
                      <td width="59">Unit</td>   
                      <td>Remarks</td>                                  
                    </tr>
                    <tr>
                        <td height="34"><input name="txtMaterialCode" type="text" class="default" id="txtMaterialCode" value="<?=$MaterialCode;?>" size="15" readonly="readonly" />
                        <? if(!$iseditline){
                        echo "<input name=\"button3\" type=\"button\" class=\"default_botton\" id=\"button3\" value=\"...\" onclick=\"showmaterial_list();\" />";
                         } ?>                        </td>   
             			 <td>
                            <input name="txtMaterialName" type="text" class="default_auto" id="txtMaterialName" value="<?=$MaterialName;?>" size="25" readonly="readonly"/></td>  
                        <td>
                            <input name="txtMaterialType" type="text" class="default_auto" id="txtMaterialType" value="<?=$MaterialType;?>" size="15"  readonly="readonly"/></td>  
                        <td>
                            <input name="txtMaterialQP" type="text" class="default" id="txtMaterialQP" value="<?=$MaterialQP;?>" size="6" onchange="calculate_total();"/></td>  
                        <td>
                            <input name="txtUnit" type="text" class="default_auto" id="txtUnit" value="<?=$Unit;?>" size="6" readonly="readonly"/></td> 
                        <td width="180">
                        <input name="txtRemarks" type="text" class="default" id="txtRemarks" value="<?=$Remarks;?>" size="30" /></td> 
                    </tr>
                </table>
                 <?
                    if($iseditline){
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Edit\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"cancel\" onclick=\"cancel_edit();\");\" />"; 
				 
					}else{
						echo "<input name=\"btn_add\" type=\"submit\" class=\"default_botton\" id=\"btn_add\" value=\"Add\" onclick=\"return(check_blank());\"/>";
						echo "<input name=\"button5\" type=\"button\" class=\"default_botton\" id=\"button5\" value=\"Clear\" onclick=\"clear_app();\" />"; 
					}
				 ?>                </td>
            </tr>
        </table>
        </form>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
