<?
	// index page
	session_start();
	include "connect.inc.php";
	session_unset();
	
	// delete transaction
	if($_GET["delete"]){
		$del_mo = $_GET["delete"];
		$sql = "select * from formula where FormulaNo='$del_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$sql = "delete from formulalineitem where FormulaNo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			$sql = "delete from formula where FormulaNo='$del_mo'";
			mysql_query($sql, $sqlconn);
			
			header("Location: mo_view.php");
		}
	}
	
	$get_mo = $_GET["mono"];
	
	if($get_mo!=""){
		$sql = "select FormulaNo,FormulaName,Date,ProductCode from formula where FormulaNo='$get_mo'";
		$result = mysql_query($sql, $sqlconn);
		if(mysql_num_rows($result)){
			$_SESSION["MoNo"] = $get_mo;
			list($MONo,$Name,$Date,$ProductCode) = mysql_fetch_row($result);
			
			$sql = "select Name from Product where ProductCode = '$ProductCode'";
			$res = mysql_query($sql,$sqlconn);
			list($ProductName) = mysql_fetch_row($res);

			$Date=strftime("%d/%m/%Y",strtotime($Date));
			
			$menu_mode = "open";	
		}
	}else{
		$menu_mode = "new";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Management :: Formula</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href="CalendarControl.css" type="text/css" rel="stylesheet" />
<script src="CalendarControl.js" language="javascript"></script>
<script language="javascript">

function showmo_list() {
	window.open("select_mo.php", "MO", "width=320 height=600");
}

function delete_mo(mono){
	var answer = confirm("Do you want to delete MONo # " + mono);
	if(answer){
		document.location.href = "mo_view.php?delete="+mono;
	}
	return false;
}

</script>
</head>

<body>
<table width="800" border="0" align="left" cellpadding="0" cellspacing="0" class="border_color">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/app_header.png" width="800" height="150" border="0" /></td>
      </tr>
      <tr>
        <td height="79" align="center">
        <table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50">
            <div align="center"><a href="mo_edit.php"><img src="images/new_page.png" width="48" height="48" border="0" /></a></div>
            </td>
            <? if($menu_mode == "open"){ ?>
            <td width="50"><div align="center">
            	<a href="mo_edit.php?mono=<?=$_SESSION["MoNo"]?>">
            		<img src="images/Modify.png" width="48" height="48" border="0" />
                </a>
            </div></td>
            <td width="50">
            <div align="center">
            	<a href="#">
       		    <img src="images/Delete.png" width="48" height="48" border="0" onclick="delete_mo('<?=$_SESSION["MoNo"]?>');" />
                </a>
            </div></td>
            <? } ?>
            <td width="50"><div align="center"><a href="report.php"><img src="images/Print.png" width="48" height="48" border="0" /></a></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">New</div></td>
            <? if($menu_mode == "open"){ ?>
            <td><div align="center">Edit</div></td>
            <td><div align="center">Delete</div></td>
            <? } ?>
            <td><div align="center">Report</div></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td><br />
        <table width="96%" height="75" border="0" align="center" cellpadding="0" cellspacing="0" class="header_field">
          <tr>
            <td width="18%" height="25">Formula Number :</td>
            <td width="26%">
              <input name="txtTranNo" type="text" class="default" id="txtTranNo" value="<?=$MONo;?>" readonly="readonly" />
              <input name="button" type="submit" class="default_botton" id="button" value="..." onclick="showmo_list();" readonly="readonly" /></td>
            <td width="13%"> Date : </td>
            <td width="43%">
              <input name="txtDate" type="text" class="default" id="txtDate" value="<?=$Date;?>" readonly="readonly"/></td>
         </tr>
          <tr>
            <td height="25">Formula  Name :</td>
            <td><input name="txtStoreName" type="text" class="default" id="txtStoreName" value="<?=$Name;?>" size="40" readonly="readonly" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="25">Product Code :</td>
            <td><input name="t4extfield" type="text" class="default" id="textfield" value="<?=$ProductCode;?>" /></td>
            <td>Product Name :</td>
            <td><input name="textfield2" type="text" class="default" id="textfield2" value="<?=$ProductName;?>" size="35" /></td>
          </tr>
          </table>
          <br />
          <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr class="show_header_table">
              <td width="5%" height="32" align="center">&nbsp;#</td>
              <td width="10%" align="center">Material Code</td>
              <td width="21%">Name</td>
              <td width="10%" align="center">Type</td>
              <td width="12%" align="center">Quantity</td>
              <td width="7%" align="center">Unit</td>
              <td width="19%" align="center">Remarks</td>
              </tr>
            <?
            	$sql = "select F.ProductCode,F.Quantity,F.Remarks".
				       ",P.Name,P.MaterialType,P.Unit from formulalineitem F join Product P on F.ProductCode = P.ProductCode ".
					   "where F.FormulaNo = '$MONo'";
				$result = mysql_query($sql,$sqlconn);
				$style = "even";  // Init
				$count = 0;
				while($data = mysql_fetch_assoc($result)){
					// swap color
					if($style == "odd") $style = "even";
					else $style = "odd";
					
					$count ++;
			?>
            <tr class="<?=$style;?>">
              <td height="28" align="center">&nbsp;<?=$count;?></td>
              <td align="center"><?=$data["ProductCode"]?></td>
              <td>&nbsp;<?=$data["Name"]?></td>
              <td align="center"><?=$data["MaterialType"]?></td>
              <td align="right"><?=$data["Quantity"]?></td>
              <td align="center"><?=$data["Unit"]?></td>
              <td align="center"><?=$data["Remarks"]?></td>
              </tr>
            <? } ?>
          </table>
          <br /></td>
      </tr>
      <tr>
        <td>© 2010. Production</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
