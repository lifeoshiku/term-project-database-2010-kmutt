<?
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="menu_style.css" />
<title>Menu</title>
<script language="javascript" type="text/javascript">
function select_op(txt){
	var md = document.getElementById("current");
	md.innerHTML = "&raquo; "+txt;
}
</script>
</head>

<body>
<img src="Menu.png" width="200" height="106" /><br />
<table width="95%" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" class="header">Current Select ::</td>
  </tr>
  <tr valign="top">
    <td height="38">&nbsp;</td>
    <td>&nbsp;</td>
    <td><div id="current" style="font-size:12px;"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="header">Production Menu ::</td>
  </tr>
  <tr>
    <td width="6%" height="10"></td>
    <td width="8%"></td>
    <td width="86%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><a href="1_formula/mo_view.php" target="mainFrame" onclick="select_op('Production Bill of Material Fomular');">&raquo; Production Bill of Material Fomular</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><a href="2_mo_order/mo_view.php" target="mainFrame"  onclick="select_op('Manufacturing Order');">&raquo; Manufacturing Order</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><i>Goods Move in Line</i></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><a href="3_flow_input/mo_view.php" target="mainFrame" onclick="select_op('Goods Move in Line (Input)');">&raquo; Input</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><a href="4_flow_output/output_view.php" target="mainFrame" onclick="select_op('Goods Move in Line (Output)');">&raquo; Output</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><a href="5_flow_use/mo_view.php" target="mainFrame" onclick="select_op('Goods Move in Line (USE)');">&raquo; Use</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><a href="6_flow_return/mo_view.php" target="mainFrame" onclick="select_op('Goods Move in Line (Return)');">&raquo; Return</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><a href="7_qa/qa_view.php" target="mainFrame"  onclick="select_op('QA for WIP/Product Goods');">&raquo; QA for WIP/Product Goods</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><a href="8_receive_qa/mo_view.php" target="mainFrame"  onclick="select_op('Receiving QA for PO/WO');">&raquo; Receiving QA for PO/WO</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><a href="9_goodshold/MO_view.php" target="mainFrame"  onclick="select_op('Goods Hold');">&raquo; Goods Hold</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

</table>
</body>
</html>
