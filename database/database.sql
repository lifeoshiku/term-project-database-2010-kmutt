-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.33-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table production_erp.flow: ~2 rows (approximately)
DELETE FROM `flow`;
/*!40000 ALTER TABLE `flow` DISABLE KEYS */;
INSERT INTO `flow` (`FlowNo`, `Date`, `Reason`, `Location`) VALUES
	('INP1000', '2017-02-02', 'Input', 'TH'),
	('INP1001', '2017-02-02', 'Input', 'TH');
/*!40000 ALTER TABLE `flow` ENABLE KEYS */;

-- Dumping data for table production_erp.flowlineitem: ~1 rows (approximately)
DELETE FROM `flowlineitem`;
/*!40000 ALTER TABLE `flowlineitem` DISABLE KEYS */;
INSERT INTO `flowlineitem` (`FlowNo`, `MONo`, `MaterialCode`, `Quantity`, `Remarks`) VALUES
	('INP1001', 'MO1000', '0453083160', 5, '-');
/*!40000 ALTER TABLE `flowlineitem` ENABLE KEYS */;

-- Dumping data for table production_erp.formula: ~2 rows (approximately)
DELETE FROM `formula`;
/*!40000 ALTER TABLE `formula` DISABLE KEYS */;
INSERT INTO `formula` (`FormulaNo`, `FormulaName`, `Date`, `ProductCode`) VALUES
	('FM', 'Part-012000005', '2017-02-02', 'PC-001'),
	('FM1', 'FM-012354123', '2017-02-02', 'PC-001');
/*!40000 ALTER TABLE `formula` ENABLE KEYS */;

-- Dumping data for table production_erp.formulalineitem: ~6 rows (approximately)
DELETE FROM `formulalineitem`;
/*!40000 ALTER TABLE `formulalineitem` DISABLE KEYS */;
INSERT INTO `formulalineitem` (`FormulaNo`, `ProductCode`, `Quantity`, `Remarks`) VALUES
	('FM', '0199002651 ', 1, '-'),
	('FM', '0453083160', 1, '-'),
	('FM', '2306488000 ', 1, '-'),
	('FM', '3015600007 ', 1, '-'),
	('FM1', '0199002651 ', 1, '-'),
	('FM1', '0453083160', 1, '-');
/*!40000 ALTER TABLE `formulalineitem` ENABLE KEYS */;

-- Dumping data for table production_erp.goodhold: ~0 rows (approximately)
DELETE FROM `goodhold`;
/*!40000 ALTER TABLE `goodhold` DISABLE KEYS */;
/*!40000 ALTER TABLE `goodhold` ENABLE KEYS */;

-- Dumping data for table production_erp.goodholdlineitem: ~0 rows (approximately)
DELETE FROM `goodholdlineitem`;
/*!40000 ALTER TABLE `goodholdlineitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `goodholdlineitem` ENABLE KEYS */;

-- Dumping data for table production_erp.location: ~1 rows (approximately)
DELETE FROM `location`;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` (`Location`, `LocationName`) VALUES
	('TH', 'Thailand');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;

-- Dumping data for table production_erp.molineitem: ~4 rows (approximately)
DELETE FROM `molineitem`;
/*!40000 ALTER TABLE `molineitem` DISABLE KEYS */;
INSERT INTO `molineitem` (`MONo`, `ProductCode`, `QuantityPer`, `TotalQuantity`, `Remarks`) VALUES
	('MO1000', '0199002651 ', 1, 30, '-'),
	('MO1000', '0453083160', 1, 30, '-'),
	('MO1000', '2306488000 ', 1, 30, '-'),
	('MO1000', '3015600007 ', 1, 30, '-');
/*!40000 ALTER TABLE `molineitem` ENABLE KEYS */;

-- Dumping data for table production_erp.morder: ~1 rows (approximately)
DELETE FROM `morder`;
/*!40000 ALTER TABLE `morder` DISABLE KEYS */;
INSERT INTO `morder` (`MONo`, `ProductCode`, `Date`, `Quantity`, `Status`) VALUES
	('MO1000', 'PC-001', '2017-02-02', 30, 'Process');
/*!40000 ALTER TABLE `morder` ENABLE KEYS */;

-- Dumping data for table production_erp.product: ~5 rows (approximately)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`ProductCode`, `Name`, `MaterialType`, `Unit`, `Type`) VALUES
	('0199002651 ', 'CPU AMD AM3+ FX-8320 3.5 GHz', 'CPU', 'Pcs', 'Material'),
	('0453083160', '3.0 TB SEAGATE SATA-III 64MB', 'HDD', 'Pcs', 'Material'),
	('2306488000 ', 'KEYBOARD FNATIC RUSH RED SW US-LG', 'Keyboard', 'Pcs', 'Material'),
	('3015600007 ', 'ATX CASE LIAN-LI PC-K6 (G99.K6SX0.00)', 'Case', 'Pcs', 'Material'),
	('PC-001', 'Computer #1', 'Computer', 'P', 'Product');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping data for table production_erp.purchaseorder: ~0 rows (approximately)
DELETE FROM `purchaseorder`;
/*!40000 ALTER TABLE `purchaseorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchaseorder` ENABLE KEYS */;

-- Dumping data for table production_erp.qa: ~0 rows (approximately)
DELETE FROM `qa`;
/*!40000 ALTER TABLE `qa` DISABLE KEYS */;
/*!40000 ALTER TABLE `qa` ENABLE KEYS */;

-- Dumping data for table production_erp.qalineitem: ~0 rows (approximately)
DELETE FROM `qalineitem`;
/*!40000 ALTER TABLE `qalineitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `qalineitem` ENABLE KEYS */;

-- Dumping data for table production_erp.receivingqa: ~0 rows (approximately)
DELETE FROM `receivingqa`;
/*!40000 ALTER TABLE `receivingqa` DISABLE KEYS */;
/*!40000 ALTER TABLE `receivingqa` ENABLE KEYS */;

-- Dumping data for table production_erp.receivingqalineitem: ~0 rows (approximately)
DELETE FROM `receivingqalineitem`;
/*!40000 ALTER TABLE `receivingqalineitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `receivingqalineitem` ENABLE KEYS */;

-- Dumping data for table production_erp.runningnumber: ~9 rows (approximately)
DELETE FROM `runningnumber`;
/*!40000 ALTER TABLE `runningnumber` DISABLE KEYS */;
INSERT INTO `runningnumber` (`LastNumber`, `RunTable`) VALUES
	(1000, 'FM'),
	(1001, 'MO'),
	(1002, 'INP'),
	(1000, 'OUT'),
	(1000, 'US'),
	(1000, 'RET'),
	(1000, 'QA'),
	(1000, 'RQA'),
	(1000, 'HD');
/*!40000 ALTER TABLE `runningnumber` ENABLE KEYS */;

-- Dumping data for table production_erp.workorder: ~0 rows (approximately)
DELETE FROM `workorder`;
/*!40000 ALTER TABLE `workorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `workorder` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
